/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DBjava;

/**
 *
 * @author ADMIN
 */
public class Item {

    String itemCode = "";
    String itemName = "";
    Supplier supplier = null;
    String unit = "";
    int price = 0;
    boolean supplying = false;

    public Item(String itemCode, String itemName, Supplier supplier, String unit, int price, boolean supplying) {
        this.itemCode = itemCode;
        this.itemName = itemName;
        this.supplier = supplier;
        this.unit = unit;
        this.price = price;
        this.supplying = supplying;
    }

    public Item() {
    }

    public String getItemCode() {
        return itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public String getUnit() {
        return unit;
    }

    public int getPrice() {
        return price;
    }

    public boolean isSupplying() {
        return supplying;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setSupplying(boolean supplying) {
        this.supplying = supplying;
    }

}
