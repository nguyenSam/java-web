/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manager;

import DBjava.Supplier;
import DAO.SupplierDAO;
import DBjava.Item;
import DAO.ItemDAO;
import Table.ItemFullModel;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import Table.SupplierFullModel;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import validator.EmpValidator.vn.util.validator.inputEmpValidator;

public class ManagerProgram extends javax.swing.JFrame {

    SupplierDAO suppliers;
    ItemDAO items;
    Table.ItemFullModel itemModel;
    SupplierFullModel supplierModel;
    boolean addNewItem = false;
    boolean addNewSupplier = false;
    Border defaultBorder = null;
    //khi mọi dữ liệu nhập vào mà ko có lỗi thì defaultBorder sẽ đc hiện lên
    Border redBorder = null;

    public ManagerProgram() {
        try {
            initComponents();
            defaultBorder = txtSupCode.getBorder();
            //biến defaultBoder sẽ chứa boder của txtCode
            redBorder = BorderFactory.createLineBorder(Color.red, 1);
            //jTabbedPane1.setSize(this.getSize().width - 10, this.getSize().height - 30);
            //addNewItem = true;
            //   addNewSupplier = true;
            suppliers = new SupplierDAO();
            suppliers.loadFromDB();
            supplierModel = new SupplierFullModel(suppliers);
            items = new ItemDAO();
            int getAll = 3;
            items.loadFromDB(suppliers, getAll);
            itemModel = new ItemFullModel(items);
            setupModel();
            startUp();
        } catch (SQLException ex) {
            Logger.getLogger(ManagerProgram.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void setupModel() {
        tblItems.setModel(itemModel);
        tblSuppliers.setModel(supplierModel);
        this.cbSuppliers.setModel(new DefaultComboBoxModel(suppliers));
    }

    private void startUp() {
        //  btnNew.setEnabled(false);
        btnRemove.setEnabled(false);
        btnSave.setEnabled(false);
        // btnNewSup.setEnabled(false);
        btnRemoveSup.setEnabled(false);
        btnSaveSup.setEnabled(false);
    }

    private void endUpItem() {
        // btnNew.setEnabled(true);
        btnRemove.setEnabled(true);
        btnSave.setEnabled(true);
    }

    private void endpUpSup() {
        // btnNewSup.setEnabled(true);
        btnRemoveSup.setEnabled(true);
        btnSaveSup.setEnabled(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSlider1 = new javax.swing.JSlider();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        txtSupCode = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtSupName = new javax.swing.JTextField();
        Add = new javax.swing.JLabel();
        txtAddSup = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        ckSupCol = new javax.swing.JCheckBox();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblSuppliers = new javax.swing.JTable();
        btnNewSup = new javax.swing.JButton();
        btnSaveSup = new javax.swing.JButton();
        btnRemoveSup = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtCode = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        cbSuppliers = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        txtUnit = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtPrice = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        ckSupplying = new javax.swing.JCheckBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblItems = new javax.swing.JTable();
        btnNew = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        btnRemove = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Item Details"));
        jPanel4.setLayout(new java.awt.GridLayout(1, 0));

        jLabel7.setText("Code");
        jPanel4.add(jLabel7);
        jPanel4.add(txtSupCode);

        jLabel8.setText("Name");
        jPanel4.add(jLabel8);
        jPanel4.add(txtSupName);

        Add.setText("Address");
        jPanel4.add(Add);
        jPanel4.add(txtAddSup);

        jLabel12.setText("Colloborating");
        jPanel4.add(jLabel12);
        jPanel4.add(ckSupCol);

        jScrollPane2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jScrollPane2MouseClicked(evt);
            }
        });

        tblSuppliers.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblSuppliers.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblSuppliersMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblSuppliers);

        btnNewSup.setText("New");
        btnNewSup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewSupActionPerformed(evt);
            }
        });

        btnSaveSup.setText("Save");
        btnSaveSup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveSupActionPerformed(evt);
            }
        });

        btnRemoveSup.setText("Delete");
        btnRemoveSup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveSupActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 441, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(31, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(btnNewSup)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnSaveSup)
                        .addGap(56, 56, 56)
                        .addComponent(btnRemoveSup)
                        .addGap(32, 32, 32))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnNewSup)
                            .addComponent(btnSaveSup)
                            .addComponent(btnRemoveSup)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 267, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(49, 49, 49))
        );

        jTabbedPane1.addTab("Manager Suppliers", jPanel1);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Item Lists"));

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Item Details"));
        jPanel3.setLayout(new java.awt.GridLayout(6, 6));

        jLabel1.setText("Code");
        jPanel3.add(jLabel1);
        jPanel3.add(txtCode);

        jLabel3.setText("Name");
        jPanel3.add(jLabel3);
        jPanel3.add(txtName);

        jLabel2.setText("Supplier");
        jPanel3.add(jLabel2);

        jPanel3.add(cbSuppliers);

        jLabel4.setText("Unit");
        jPanel3.add(jLabel4);
        jPanel3.add(txtUnit);

        jLabel5.setText("Price");
        jPanel3.add(jLabel5);
        jPanel3.add(txtPrice);

        jLabel6.setText("Supplying");
        jPanel3.add(jLabel6);
        jPanel3.add(ckSupplying);

        tblItems.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblItems.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblItemsMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblItemsMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tblItems);

        btnNew.setText("New");
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });

        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnRemove.setText("Delete");
        btnRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 441, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(19, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(btnNew)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnSave)
                        .addGap(56, 56, 56)
                        .addComponent(btnRemove)
                        .addGap(32, 32, 32))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnNew)
                            .addComponent(btnSave)
                            .addComponent(btnRemove)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 267, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(49, 49, 49))
        );

        jTabbedPane1.addTab("Manage Items", jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 823, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 48, Short.MAX_VALUE)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 383, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tblItemsMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblItemsMouseReleased
        int row = tblItems.getSelectedRow();
        int col = tblItems.getSelectedColumn();
        if (row >= 0 && col >= 0) {
            tblItems.getCellEditor(row, col).cancelCellEditing();
        }
    }//GEN-LAST:event_tblItemsMouseReleased

    private void tblItemsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblItemsMouseClicked
        addNewItem = false;
        int pos = tblItems.getSelectedRow();
        Item item = itemModel.getItems().get(pos);
        txtCode.setText(item.getItemCode());
        txtCode.setEditable(false);
        txtName.setText(item.getItemName());
        int index = suppliers.find(item.getSupplier().getSupCode());
        cbSuppliers.setSelectedIndex(index);
        txtUnit.setText("" + item.getUnit());
        txtPrice.setText("" + item.getPrice());
        ckSupplying.setSelected(item.isSupplying());
        endUpItem();
    }//GEN-LAST:event_tblItemsMouseClicked

    private void btnRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveActionPerformed
        int pos = tblItems.getSelectedRow();
        String itemCode = txtCode.getText();
        int n;
        ItemDAO dao = new ItemDAO();
        // String sql = "Delete from items where itemcode=?";
        // JOptionPane.showMessageDialog(this, sql);
        String msg = "The item " + itemCode + " has been deleted from DB!";
        int result = JOptionPane.showConfirmDialog(this, "Delete this item?", "Delete", JOptionPane.YES_NO_OPTION);
        if (result == JOptionPane.YES_OPTION) {
            try {
                n = dao.delete(itemCode);
                if (n > 0) {
                    JOptionPane.showMessageDialog(this, msg);
                    itemModel.getItems().removeElementAt(pos);
                    tblItems.updateUI();
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, e);
            }
        }
    }//GEN-LAST:event_btnRemoveActionPerformed

    private void btnNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed
        addNewItem = true;
        txtCode.setText("");
        txtCode.setEditable(true);
        txtCode.requestFocus();
        txtName.setText("");
        cbSuppliers.setSelectedIndex(0);
        txtUnit.setText("");
        txtPrice.setText("");
        ckSupplying.setSelected(true);
        endUpItem();
    }//GEN-LAST:event_btnNewActionPerformed
    private String checkItemCode(String code) {
        if (code == null) {
            JOptionPane.showMessageDialog(this, "Code not null");
        }
        return code;
    }

    private int checkPrice(String price) {
        int realcode = 0;
        try {
            realcode = Integer.parseInt(price);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e);
        }
        return realcode;
    }

//    private boolean validDataForItems(String price) {
////        if (!inputEmpValidator.checkCode(Code)) {
////            JOptionPane.showMessageDialog(this, "Code not null");
////            txtCode.requestFocus();
////            txtCode.setBorder(redBorder);
////            return false;
////        } else {
////            txtCode.setBorder(defaultBorder);
////            txtCode.updateUI();
////
////        }
//        if (!inputEmpValidator.checkPrice(price)) {
//            JOptionPane.showMessageDialog(this, "Invalid Price");
//            txtPrice.requestFocus();
//            txtPrice.setBorder(redBorder);
//            return false;
//        } else {
//            txtPrice.setBorder(defaultBorder);
//            txtPrice.updateUI();
//        }
//        return true;
//    }

//    private boolean validDataForSup(String code) {
//        if (!inputEmpValidator.checkCode(code)) {
//            JOptionPane.showMessageDialog(this, "Code not null");
//            txtSupCode.requestFocus();
//            txtSupCode.setBorder(redBorder);
//            return false;
//        } else {
//            txtSupCode.setBorder(defaultBorder);
//            txtSupCode.updateUI();
//        }
//        return true;
//    }
    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed

        String itemCode = txtCode.getText().trim();
        if(itemCode.equals("")){
            txtCode.requestFocus();
            txtCode.setBorder(redBorder);
            return;
        }
        txtCode.setBorder(defaultBorder);
        String itemName = txtName.getText().trim();
        Supplier supplier = (Supplier) cbSuppliers.getSelectedItem();
        String supCode = supplier.getSupCode();
        String unit = txtUnit.getText().trim();
        String number=txtPrice.getText().trim();
        if(!number.matches("\\d+")){
            txtPrice.requestFocus();
            txtPrice.setBorder(redBorder);
             JOptionPane.showMessageDialog(this, "Only accept number");
            return;
           
        }
        txtPrice.setBorder(defaultBorder);
        int price = Integer.parseInt(txtPrice.getText());
        boolean supplying = ckSupplying.isSelected();
        int n = 0;
        ItemDAO dao = new ItemDAO();
        try {
            //Item item = new Item(itemCode, itemName, supplier, unit, price, supplying);
            if (addNewItem == true) {
                n = dao.insert(itemCode, itemName, supCode, unit, price, supplying);
            } else {
                n = dao.update(itemCode, itemName, supCode, unit, price, supplying);
            }
            //JOptionPane.showMessageDialog(this, sql);
            String msg;
            //int n = dbAccess.executeUpdate(sql);
            if (n > 0) {
                //JOptionPane.showMessageDialog(this, msg);
                if (addNewItem == false) {
                    msg = "An item has been updated";
                    Item item = new Item(itemCode, itemName, supplier, unit, price, supplying);
                    int pos = tblItems.getSelectedRow();
                    itemModel.getItems().set(pos, item);
                } else {
                    msg = "An item has been added";
                    Item item = new Item(itemCode, itemName, supplier, unit, price, supplying);
                    itemModel.getItems().add(item);
                }
                JOptionPane.showMessageDialog(this, msg);
                tblItems.updateUI();

            }
            tblItems.updateUI();
            addNewItem = false;
        }catch(ArrayIndexOutOfBoundsException e){
            JOptionPane.showMessageDialog(this, "You must click to update");
        }catch(NumberFormatException e){
            JOptionPane.showMessageDialog(this, "Only accept number");
        }
        catch (Exception e) {
            JOptionPane.showMessageDialog(this, e);
            // e.printStackTrace();
        }
        // endUpItem();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnNewSupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewSupActionPerformed
        addNewSupplier = true;
        txtSupCode.setEditable(true);
        txtSupCode.setText("");
        txtSupName.setText("");
        txtAddSup.setText("");
        txtSupCode.requestFocus();
        ckSupCol.setSelected(true);
        endpUpSup();
    }//GEN-LAST:event_btnNewSupActionPerformed

    private void btnSaveSupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveSupActionPerformed
       
            String code = this.txtSupCode.getText().trim();
            if(code.equals("")){
                txtSupCode.requestFocus();
                txtSupCode.setBorder(redBorder);
                JOptionPane.showMessageDialog(this, "Null man");
                return;
            }
            txtSupCode.setBorder(defaultBorder);
            String name = txtSupName.getText().trim();
            String add = txtAddSup.getText().trim();
            boolean collo = ckSupCol.isSelected();
            int n;
            SupplierDAO dao = new SupplierDAO();
            try {
                if (addNewSupplier == true) {
                    n = dao.insert(code, name, add, collo);
                } else {
                    n = dao.update(code, name, add, collo);
                }
                String msg;
                if (n > 0) {
                    if (addNewSupplier == true) {
                        msg = "New supplier have been added";
                        Supplier sup = new Supplier(code, name, add, collo);
                        supplierModel.getSup().add(sup);
                    } else {
                        msg = "Supplier " + code + " have been updated";
                        Supplier sup = new Supplier(code, name, add, collo);
                        int pos = tblSuppliers.getSelectedRow();
                        supplierModel.getSup().set(pos, sup);
                    }
                    JOptionPane.showMessageDialog(this, msg);
                    tblSuppliers.updateUI();

                }
                tblSuppliers.updateUI();
                // endpUpSup();
                addNewSupplier = false;
            }catch(ArrayIndexOutOfBoundsException e){
                JOptionPane.showMessageDialog(this, "You must click if you want to update");
            }
                    
            catch (Exception e) {
                JOptionPane.showMessageDialog(this, e);
            }
        
    }//GEN-LAST:event_btnSaveSupActionPerformed

    private boolean checkCodeSupplier(String code) {
        for (int i = 0; i < suppliers.size(); i++) {
            Supplier tmp = suppliers.get(i);
            if (tmp.getSupCode().equals(code)) {
                return false;
            }
        }
        return true;
    }
    private void btnRemoveSupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveSupActionPerformed

        int pos = tblSuppliers.getSelectedRow();
        // Supplier tmp = suppliers.get(pos);
        String code = txtSupCode.getText();
        SupplierDAO dao = new SupplierDAO();
        // String sql = "Delete from Suppliers where SupCode='" + code + "'";
        // JOptionPane.showMessageDialog(this, sql);
        String msg = "The supplier " + code + " has been deleted from DB!";
        int result = JOptionPane.showConfirmDialog(this, "Delete this supplier?", "Delete", JOptionPane.YES_NO_OPTION);
        if (result == JOptionPane.YES_OPTION) {
            try {
                int n = dao.delete(code);
                if (n > 0) {
                    JOptionPane.showMessageDialog(this, msg);
                    supplierModel.getSup().remove(pos);
                    tblSuppliers.updateUI();
                    // btnNewSupActionPerformed(evt);
                } else {
                    JOptionPane.showMessageDialog(this, "you this not choosing anything");
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, e);
            }
        }

    }//GEN-LAST:event_btnRemoveSupActionPerformed

    private void tblSuppliersMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblSuppliersMouseClicked
        int pos = tblSuppliers.getSelectedRow();
        Supplier tmp = suppliers.get(pos);
        txtSupCode.setText(tmp.getSupCode());
        txtSupName.setText(tmp.getSupName());
        txtAddSup.setText(tmp.getAddress());
        txtSupCode.setEditable(false);
        endpUpSup();
    }//GEN-LAST:event_tblSuppliersMouseClicked

    private void jScrollPane2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jScrollPane2MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jScrollPane2MouseClicked

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ManagerProgram.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ManagerProgram.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ManagerProgram.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ManagerProgram.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ManagerProgram().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Add;
    private javax.swing.JButton btnNew;
    private javax.swing.JButton btnNewSup;
    private javax.swing.JButton btnRemove;
    private javax.swing.JButton btnRemoveSup;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnSaveSup;
    private javax.swing.JComboBox<String> cbSuppliers;
    private javax.swing.JCheckBox ckSupCol;
    private javax.swing.JCheckBox ckSupplying;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSlider jSlider1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable tblItems;
    private javax.swing.JTable tblSuppliers;
    private javax.swing.JTextField txtAddSup;
    private javax.swing.JTextField txtCode;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtPrice;
    private javax.swing.JTextField txtSupCode;
    private javax.swing.JTextField txtSupName;
    private javax.swing.JTextField txtUnit;
    // End of variables declaration//GEN-END:variables
}
