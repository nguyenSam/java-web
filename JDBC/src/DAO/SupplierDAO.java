/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DBjava.Supplier;
import dbconnection.DBConnection;
import java.util.Vector;

import java.sql.*;
import javax.swing.JOptionPane;

public class SupplierDAO extends Vector<Supplier> {

    public SupplierDAO() {
        super();
    }

    public int find(String supCode) {
        for (int i = 0; i < this.size(); i++) {
            if (supCode.equals(this.get(i).getSupCode())) {
                return i;
            }

        }
        return -1;
    }

    public Supplier findSupllier(String supCode) {
        int i = find(supCode);
        return i < 0 ? null : this.get(i);
    }

    public void loadFromDB() {
        String supCode;
        String supName;
        String address;
        boolean colloborating;
        Connection conn=null;
        PreparedStatement pre=null;
        ResultSet rs=null;
        String sql = "select * from Suppliers";
        try {
            conn=dbconnection.DBConnection.getConnection();
            pre=conn.prepareStatement(sql);
            rs=pre.executeQuery();
            // rs = dbObj.executeQuery(sql);
            while (rs.next()) {
                supCode = rs.getString(1);
                supName = rs.getString(2);
                address = rs.getString(3);
                colloborating = rs.getBoolean(4);
                Supplier supplier = new Supplier(supCode, supName, address, colloborating);
                this.add(supplier);
            }
            rs.close();
            pre.close();
            conn.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
public int update(String supcode, String supname, String supadd, boolean collo) throws SQLException, ClassNotFoundException {
        Connection conn = null;
        int result = -1;
        PreparedStatement pre = null;
        try {
            //sql = "select Supcode from Suppliers";
            String sql = "update Suppliers set SupName=?, Address=?,colloborating=? where SupCode=?";
            conn = DBConnection.getConnection();
            pre = conn.prepareStatement(sql);
            pre.setString(1, supname);
            pre.setString(2, supadd);
            pre.setBoolean(3, collo);
            pre.setString(4, supcode);
            result = pre.executeUpdate();
        } finally {

            if (pre != null) {
                pre.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return result;
    }
public int insert(String supcode, String supname, String supadd, boolean collo) throws SQLException, ClassNotFoundException {
        Connection conn = null;
        int result = -1;
        PreparedStatement pre = null;
        try {
            String sql = "insert Suppliers values(?,?,?,?)";
            conn = DBConnection.getConnection();
            pre = conn.prepareStatement(sql);
            pre.setString(1, supcode);
            pre.setString(2, supname);
            pre.setString(3, supadd);
            pre.setBoolean(4, collo);
            result = pre.executeUpdate();
        } finally {

            if (pre != null) {
                pre.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return result;
    }
public int delete(String supCode) throws SQLException, ClassNotFoundException{
    Connection conn=null;
    int result=-1;
    PreparedStatement pre=null;
    try {
        String sql="delete from Suppliers where SupCode=?";
        conn=DBConnection.getConnection();
        pre=conn.prepareCall(sql);
        pre.setString(1, supCode);
        result=pre.executeUpdate();
    } finally{
        if(pre!=null){
            pre.close();
        }
        if(conn!=null){
            conn.close();
        }
    }
    return result;
}
}
