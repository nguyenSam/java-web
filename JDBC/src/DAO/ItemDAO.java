/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.SupplierDAO;
import DBjava.Item;
import DBjava.Supplier;
import DBjava.Supplier;
import java.util.Vector;

import java.sql.*;
import javax.swing.JOptionPane;

public class ItemDAO extends Vector<Item> {

    final int SUPPLYING = 1;
    final int NOTSUPPLING = 2;

    public ItemDAO() {

    }

    

    public void loadFromDB(SupplierDAO suppliers, int supply) throws SQLException {
        String itemCode, itemName, supplierCode, unit;
        int price;
        boolean supplying;
        String sql = "";
        Connection conn = null;
        PreparedStatement pre = null;
        ResultSet rs = null;
        if(supply==SUPPLYING){
            sql="select * from Items where supplying=true";
        }else if(supply==NOTSUPPLING){
            sql="select * from Items where supplying=false";
        }else{
            sql="select * from Items";
        }
        try {
           conn=dbconnection.DBConnection.getConnection();
           pre=conn.prepareStatement(sql);
           rs=pre.executeQuery();
            while (rs.next()) {                
                itemCode=rs.getString(1);
                itemName=rs.getString(2);
                supplierCode=rs.getString(3);
                Supplier supplier=suppliers.findSupllier(supplierCode);
                unit=rs.getString(4);
                price=rs.getInt(5);
                supplying=rs.getBoolean(6);
                Item item=new Item(itemCode, itemName, supplier, unit, price, supplying);
                this.add(item);
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            if(pre!=null){
                pre.close();
            }
            if(conn!=null){
                conn.close();
            }
        }
    }

    public int update(String itemCode, String itemName, String supCode, String unit, int price, boolean supply) throws SQLException, ClassNotFoundException {
        int result = -1;
        Connection conn = null;
        PreparedStatement pre = null;
        try {
            conn = dbconnection.DBConnection.getConnection();
            String sql = "update Items set itemName=?,supCode=?,unit=?,price=?,supplying=? where itemCode=?";
            pre = conn.prepareStatement(sql);
            pre.setString(1, itemName);
            pre.setString(2, supCode);
            pre.setString(3, unit);
            pre.setInt(4, price);
            pre.setBoolean(5, supply);
            pre.setString(6, itemCode);
            result = pre.executeUpdate();
        } finally {
            if (pre != null) {
                pre.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return result;

    }

    public int insert(String itemCode, String itemName, String supCode, String unit, int price, boolean supply) throws SQLException, ClassNotFoundException {
        int result = -1;
        Connection conn = null;
        PreparedStatement pre = null;
        try {
            conn = dbconnection.DBConnection.getConnection();
            String sql = "insert Items values(?,?,?,?,?,?)";
            pre = conn.prepareStatement(sql);
            pre.setString(1, itemCode);
            pre.setString(2, itemName);
            pre.setString(3, supCode);
            pre.setString(4, unit);
            pre.setInt(5, price);
            pre.setBoolean(6, supply);
            result = pre.executeUpdate();
        } finally {
            if (pre != null) {
                pre.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return result;
    }
    public int delete(String itemCode) throws SQLException, ClassNotFoundException{
        int result = -1;
        Connection conn = null;
        PreparedStatement pre = null;
        try {
            conn=dbconnection.DBConnection.getConnection();
            String sql="delete from Items where itemCode=? ";
            pre=conn.prepareStatement(sql);
            pre.setString(1, itemCode);
            result=pre.executeUpdate();
        } finally{
            if(pre!=null){
                pre.close();
            }
            if(conn!=null){
                conn.close();
            }
        }
        return result;
    }
}
