/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Table;

import DBjava.Supplier;
import DAO.SupplierDAO;
import DBjava.Item;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author PhuNDSE63159
 */
public class SupplierFullModel extends AbstractTableModel {

    SupplierDAO dao = new SupplierDAO();

    public SupplierFullModel() {
    }
 public SupplierDAO getSup(){
     return dao;
 }
    public SupplierFullModel(SupplierDAO i) {
        this.dao = i;
    }

    @Override
    public int getRowCount() {
        return dao.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Supplier sup = dao.get(rowIndex);
        Object obj = null;
        switch (columnIndex) {
            case 0:
                obj = sup.getSupCode();
                break;
            case 1:
                obj = sup.getSupName();
                break;
            case 2:
                obj = sup.getAddress();
                break;
        }
        return obj;
    }

}
