USE [master]
GO
/****** Object:  Database [ItemDB]    Script Date: 8/7/2018 4:58:51 PM ******/
CREATE DATABASE [ItemDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ItemDB', FILENAME = N'E:\Java desktop\ItemDB-SQLSrv\ItemDB.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'ItemDB_log', FILENAME = N'E:\Java desktop\ItemDB-SQLSrv\ItemDB_log.ldf' , SIZE = 3456KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [ItemDB] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ItemDB].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO
ALTER DATABASE [ItemDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ItemDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ItemDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ItemDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ItemDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [ItemDB] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [ItemDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ItemDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ItemDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ItemDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ItemDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ItemDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ItemDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ItemDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ItemDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ItemDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ItemDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ItemDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ItemDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ItemDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ItemDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ItemDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ItemDB] SET RECOVERY FULL 
GO
ALTER DATABASE [ItemDB] SET  MULTI_USER 
GO
ALTER DATABASE [ItemDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ItemDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ItemDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ItemDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [ItemDB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [ItemDB] SET QUERY_STORE = OFF
GO
USE [ItemDB]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [ItemDB]
GO
/****** Object:  Table [dbo].[Items]    Script Date: 8/7/2018 4:58:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Items](
	[itemCode] [nchar](5) NOT NULL,
	[itemName] [nvarchar](50) NULL,
	[supCode] [nvarchar](5) NULL,
	[unit] [nvarchar](10) NULL,
	[price] [int] NULL,
	[supplying] [bit] NULL,
 CONSTRAINT [PK_Items] PRIMARY KEY CLUSTERED 
(
	[itemCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Suppliers]    Script Date: 8/7/2018 4:58:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Suppliers](
	[SupCode] [nvarchar](5) NOT NULL,
	[SupName] [nvarchar](30) NULL,
	[Address] [nvarchar](30) NULL,
	[colloborating] [bit] NULL,
 CONSTRAINT [PK_Suppliers] PRIMARY KEY CLUSTERED 
(
	[SupCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Items] ([itemCode], [itemName], [supCode], [unit], [price], [supplying]) VALUES (N'E0001', N'fsf', N'GA', N'fs', 34, 0)
INSERT [dbo].[Items] ([itemCode], [itemName], [supCode], [unit], [price], [supplying]) VALUES (N'E0002', N'Keyboard Proview', N'MT', N'block 10', 22, 1)
INSERT [dbo].[Items] ([itemCode], [itemName], [supCode], [unit], [price], [supplying]) VALUES (N'E0003', N'hhh', N'MT', N'1-unit', 454, 1)
INSERT [dbo].[Items] ([itemCode], [itemName], [supCode], [unit], [price], [supplying]) VALUES (N'E0004', N'sam', N'GA', N'block 3', 25, 1)
INSERT [dbo].[Items] ([itemCode], [itemName], [supCode], [unit], [price], [supplying]) VALUES (N'E0012', N'sad', N'GA', N'afa', 24, 1)
INSERT [dbo].[Items] ([itemCode], [itemName], [supCode], [unit], [price], [supplying]) VALUES (N'E0023', N'sam', N'HT', N'block 12', 263, 1)
INSERT [dbo].[Items] ([itemCode], [itemName], [supCode], [unit], [price], [supplying]) VALUES (N'E0067', N'sdf', N'GA', N'fe', 34, 0)
INSERT [dbo].[Items] ([itemCode], [itemName], [supCode], [unit], [price], [supplying]) VALUES (N'E223 ', N'HAHU', N'GA', N'BLOCK 78', 23, 1)
INSERT [dbo].[Items] ([itemCode], [itemName], [supCode], [unit], [price], [supplying]) VALUES (N'e23  ', N'', N'23', N'', 23, 1)
INSERT [dbo].[Items] ([itemCode], [itemName], [supCode], [unit], [price], [supplying]) VALUES (N'e2333', N'logitech', N'HT', N'block 5', 36, 0)
INSERT [dbo].[Suppliers] ([SupCode], [SupName], [Address], [colloborating]) VALUES (N'23', N'', N'', 1)
INSERT [dbo].[Suppliers] ([SupCode], [SupName], [Address], [colloborating]) VALUES (N'd2', N'', N'', 1)
INSERT [dbo].[Suppliers] ([SupCode], [SupName], [Address], [colloborating]) VALUES (N'dđ', N'', N'', 1)
INSERT [dbo].[Suppliers] ([SupCode], [SupName], [Address], [colloborating]) VALUES (N'De2', N'', N'', 1)
INSERT [dbo].[Suppliers] ([SupCode], [SupName], [Address], [colloborating]) VALUES (N'dư', N'dư', N'', 1)
INSERT [dbo].[Suppliers] ([SupCode], [SupName], [Address], [colloborating]) VALUES (N'e34', N'', N'', 1)
INSERT [dbo].[Suppliers] ([SupCode], [SupName], [Address], [colloborating]) VALUES (N'GA', N'sd', N'ádasdas', 1)
INSERT [dbo].[Suppliers] ([SupCode], [SupName], [Address], [colloborating]) VALUES (N'ha', N'haha', N'44 duong', 0)
INSERT [dbo].[Suppliers] ([SupCode], [SupName], [Address], [colloborating]) VALUES (N'HT', N'a sam', N'khac', 1)
INSERT [dbo].[Suppliers] ([SupCode], [SupName], [Address], [colloborating]) VALUES (N'MT', N'haha Tr', N'37, Hai ádasda asd, Q1', 1)
ALTER TABLE [dbo].[Items]  WITH CHECK ADD  CONSTRAINT [FK_Items_Suppliers] FOREIGN KEY([supCode])
REFERENCES [dbo].[Suppliers] ([SupCode])
GO
ALTER TABLE [dbo].[Items] CHECK CONSTRAINT [FK_Items_Suppliers]
GO
USE [master]
GO
ALTER DATABASE [ItemDB] SET  READ_WRITE 
GO
