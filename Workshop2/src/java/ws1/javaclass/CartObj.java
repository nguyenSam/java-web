/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws1.javaclass;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import ws2.productdao.ProductDTO;

/**
 *
 * @author ADMIN
 */
public class CartObj implements Serializable {

    private String customerID;
    private Map<ProductDTO, Integer> mobiles;

    public Map<ProductDTO, Integer> getMobiles() {
        return mobiles;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public void addMobile(ProductDTO item) {
        if (this.mobiles == null) {
            this.mobiles = new HashMap<>();
        }
//        if (item.getMobileName() == null) {
//
//        } else {
        if (item != null) {
            int quantity = 1;
            if (this.mobiles.containsKey(item)) {
                quantity = this.mobiles.get(item) + 1;

            }
            this.mobiles.put(item, quantity);
        }

        // }
    }

    public void removeMobile(String id) {
        if (this.mobiles == null) {
            return;
        }
        ProductDTO dto = new ProductDTO(id, "", 0);
        if (this.mobiles.containsKey(dto)) {
            this.mobiles.remove(dto);
            if (this.mobiles.isEmpty()) {
                this.mobiles = null;
            }
        }
    }
}
