/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws1.tblmobiledao;

import ws1.tbluserdto.tblUserDTO;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import ws1.database.DBUtils;

/**
 *
 * @author ADMIN
 */
public class tblMobileDAO implements Serializable {

    private List<tblMobileDTO> listMobileDTO;

    public List<tblMobileDTO> getListDTO() {
        return listMobileDTO;
    }
    private List<tblUserDTO> listUserDTO;

    public List<tblUserDTO> getListUserDTO() {
        return listUserDTO;
    }

    public boolean checkLogin(String username, String password) throws SQLException, NamingException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "Select * from tblUser where userID=? and password=?";
                ps = con.prepareStatement(sql);
                ps.setString(1, username);
                ps.setString(2, password);
                rs = ps.executeQuery();
                if (rs.next()) {
                    return true;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    public void searchValue(String id, String name) throws SQLException, NamingException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "Select * from tblMobile where mobileId like ? or mobileName like ?";
                ps = con.prepareStatement(sql);
                ps.setString(1, "%" + id + "%");
                ps.setString(2, "%" + name + "%");
                rs = ps.executeQuery();
                while (rs.next()) {
                    String userId = rs.getString("mobileId");
                    String description = rs.getString("description");
                    float price = rs.getFloat("price");
                    String mobileName = rs.getString("mobileName");
                    int year = rs.getInt("yearOfProduction");
                    int quantity = rs.getInt("quantity");
                    boolean notSale = rs.getBoolean("notSale");
                    tblMobileDTO dto = new tblMobileDTO(userId, description, price, mobileName, year, quantity, notSale);
                    if (listMobileDTO == null) {
                        listMobileDTO = new ArrayList<>();
                    }
                    listMobileDTO.add(dto);

                }

            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }

    }

    public String checkRole(tblUserDTO dto) throws SQLException, NamingException {
        String username = dto.getUsername();
        String password = dto.getPassword();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "Select userID,password,role from tblUser";
                ps = con.prepareStatement(sql);

                rs = ps.executeQuery();
                while (rs.next()) {
                    String userId = rs.getString("userID");
                    String pass = rs.getString("password");
                    int role = rs.getInt("role");
                    if (userId.equals(username) && pass.equals(password) && role == 0) {
                        return "User";
                    } else if (userId.equals(username) && pass.equals(password) && role == 1) {
                        return "Admin";
                    } else if (userId.equals(username) && pass.equals(password) && role == 2) {
                        return "Manager";
                    }

                }

            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return null;
    }

    public void searchPrice(Float priceMin, Float priceMax) throws NamingException, SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "Select * from tblMobile where ? < price and price < ?";
                ps = con.prepareStatement(sql);
                ps.setFloat(1, priceMin);
                ps.setFloat(2, priceMax);
                rs = ps.executeQuery();
                while (rs.next()) {
                    String userId = rs.getString("mobileId");
                    String description = rs.getString("description");
                    float price = rs.getFloat("price");
                    String mobileName = rs.getString("mobileName");
                    int year = rs.getInt("yearOfProduction");
                    int quantity = rs.getInt("quantity");
                    boolean notSale = rs.getBoolean("notSale");
                    tblMobileDTO dto = new tblMobileDTO(userId, description, price, mobileName, year, quantity, notSale);
                    if (listMobileDTO == null) {
                        listMobileDTO = new ArrayList<>();
                    }
                    listMobileDTO.add(dto);

                }

            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }
    }

    public boolean deleteMobile(String mobileId) throws SQLException, NamingException {
        Connection con = null;
        PreparedStatement ps = null;
        //ResultSet rs = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "Delete from tblMobile where mobileId=?";
                ps = con.prepareStatement(sql);
                ps.setString(1, mobileId);
                int result = ps.executeUpdate();
                if (result > 0) {
                    return true;
                }

            }
        } finally {

            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }

        return false;
    }

    public boolean insertMobile(String id, String des, float price, String name, int year, int quantity, boolean sale) throws SQLException, NamingException {
        Connection con = null;
        PreparedStatement ps = null;
        //ResultSet rs = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "Insert into tblMobile (mobileId,description,price,mobileName,yearOfProduction,quantity,notSale)"
                        + " values(?,?,?,?,?,?,?)";
                ps = con.prepareStatement(sql);
                ps.setString(1, id);
                ps.setString(2, des);
                ps.setFloat(3, price);
                ps.setString(4, name);
                ps.setInt(5, year);
                ps.setInt(6, quantity);
                ps.setBoolean(7, sale);
                int result = ps.executeUpdate();
                if (result > 0) {
                    return true;
                }

            }
        } finally {

            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }

        return false;
    }

    public boolean updateMobile(String id, String des, float price, int quantity, boolean sale) throws SQLException, NamingException {
        Connection con = null;
        PreparedStatement ps = null;
        //ResultSet rs = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "Update tblMobile set description=?,price=?,quantity=?,notSale=? where mobileId=?";
                ps = con.prepareStatement(sql);
                ps.setString(1, des);
                ps.setFloat(2, price);
                ps.setInt(3, quantity);
                ps.setBoolean(4, sale);
                ps.setString(5, id);
                int result = ps.executeUpdate();
                if (result > 0) {
                    return true;
                }

            }
        } finally {

            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }

        return false;
    }

    public boolean storeToOrderDetails(int orderId, String mobileName, Date date, int quantity) throws SQLException, NamingException {
        Connection con = null;
        PreparedStatement ps = null;
        //ResultSet rs = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "Insert into tblOrderDetails (orderId,mobileName,dateBuy,quantity) VALUES (?,?,?,?)";
                ps = con.prepareStatement(sql);
                ps.setInt(1, orderId);
                ps.setString(2, mobileName);
                ps.setDate(3, date);
                ps.setInt(4, quantity);
                int result = ps.executeUpdate();
                if (result > 0) {
                    return true;
                }

            }
        } finally {

            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }

        return false;
    }

    public boolean storeToOrder(int orderId, String userId) throws SQLException, NamingException {
        Connection con = null;
        PreparedStatement ps = null;
        //ResultSet rs = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "Insert into tblOrder(orderId,userId) VALUES (?,?)";
                ps = con.prepareStatement(sql);
                ps.setInt(1, orderId);
                ps.setString(2, userId);
                int result = ps.executeUpdate();
                if (result > 0) {
                    return true;
                }

            }
        } finally {

            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }

        return false;
    }

    public void loadMobile()
            throws NamingException, SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "Select * from tblMobile";
                ps = con.prepareStatement(sql);
                // ps.setFloat(1, rangePrice);
                rs = ps.executeQuery();
                while (rs.next()) {
                    String userId = rs.getString("mobileId");
                    String description = rs.getString("description");
                    float price = rs.getFloat("price");
                    String mobileName = rs.getString("mobileName");
                    int year = rs.getInt("yearOfProduction");
                    int quantity = rs.getInt("quantity");
                    boolean notSale = rs.getBoolean("notSale");
                    tblMobileDTO dto = new tblMobileDTO(userId, description, price, mobileName, year, quantity, notSale);
                    if (listMobileDTO == null) {
                        listMobileDTO = new ArrayList<>();
                    }
                    listMobileDTO.add(dto);

                }

            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }
    }

}
