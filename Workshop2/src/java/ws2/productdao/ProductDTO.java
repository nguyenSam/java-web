/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws2.productdao;

import java.io.Serializable;

/**
 *
 * @author ADMIN
 */
public class ProductDTO implements Serializable {

    private String mobileID;
    private String mobileName;
    private float price;

    public ProductDTO() {
    }

    public ProductDTO(String productID, String productName, float price) {
        this.mobileID = productID;
        this.mobileName = productName;
        this.price = price;
    }

    /**
     * @return the mobileID
     */
    public String getMobileID() {
        return mobileID;
    }

    /**
     * @param mobileID the mobileID to set
     */
    public void setMobileID(String mobileID) {
        this.mobileID = mobileID;
    }

    /**
     * @return the mobileName
     */
    public String getMobileName() {
        return mobileName;
    }

    /**
     * @param mobileName the mobileName to set
     */
    public void setMobileName(String mobileName) {
        this.mobileName = mobileName;
    }

    /**
     * @return the price
     */
    public float getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ProductDTO)) {
            return false;
        }
        ProductDTO other = (ProductDTO) obj;
        if ((this.mobileID == null && other.mobileID != null) || (this.mobileID != null && !this.mobileID.equals(other.mobileID))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mobileID != null ? mobileID.hashCode() : 0);
        return hash;
    }

}
