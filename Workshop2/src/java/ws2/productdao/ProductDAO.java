/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws2.productdao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.naming.NamingException;
import ws1.database.DBUtils;

/**
 *
 * @author ADMIN
 */
public class ProductDAO implements Serializable {

    private Map<String, String> mobileList;

    public Map<String, String> getMobileList() {
        return mobileList;
    }

    public void getAllProduct() throws NamingException, SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "Select mobileID,mobileName from tblMobile";
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    String id = rs.getString("mobileID");
                    String name = rs.getString("mobileName");
                    if (this.mobileList == null) {
                        this.mobileList = new HashMap<>();

                    }
                    this.mobileList.put(id, name);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }

        }
    }

    public ProductDTO getProduct(String id) throws NamingException, SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "Select * from tblMobile where mobileID=?";
                ps = con.prepareStatement(sql);
                ps.setString(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    String name = rs.getString("mobileName");
                    float price = rs.getFloat("price");
                    ProductDTO dto = new ProductDTO(id, name, price);
                    return dto;
                }

            }

        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }

        }
        return null;
    }
}
