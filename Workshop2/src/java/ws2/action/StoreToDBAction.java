/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws2.action;

import com.opensymphony.xwork2.ActionContext;
import java.sql.Date;
import java.util.Calendar;
import java.util.Map;
import java.util.Random;
import ws1.javaclass.CartObj;
import ws1.tblmobiledao.tblMobileDAO;
import ws2.productdao.ProductDTO;

/**
 *
 * @author ADMIN
 */
public class StoreToDBAction {

    private final String SUCCESS = "success";

    public StoreToDBAction() {
    }

    public String execute() throws Exception {
        Map session = ActionContext.getContext().getSession();
        CartObj co = (CartObj) session.get("CART");
        String url = "";
        if (co != null) {
            Map<ProductDTO, Integer> mobiles = co.getMobiles();
            String userId = (String) session.get("USERNAME");
            int orderID;
            Random rd = new Random();
            tblMobileDAO dao = new tblMobileDAO();
            Date date = new Date(Calendar.getInstance().getTime().getTime());
            for (Map.Entry<ProductDTO, Integer> mobile : mobiles.entrySet()) {
                ProductDTO key = mobile.getKey();
                Integer value = mobile.getValue();
                orderID = rd.nextInt(150) + 1;
                System.out.println(orderID);
                System.out.println(userId);
                System.out.println(key.getMobileName());
                boolean result2 = dao.storeToOrderDetails(orderID, key.getMobileName(), date, value);
                boolean result = dao.storeToOrder(orderID, userId);
                // boolean result2 = dao.storeToOrderDetails(orderID, key.getMobileName(), date, value);
                if (result && result2) {

                    url = SUCCESS;
                }

            }

        }
        session.clear();
        return url;
    }

}
