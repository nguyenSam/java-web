/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws2.action;

import com.opensymphony.xwork2.ActionContext;
import java.util.Map;
import javax.servlet.http.HttpSession;
import ws1.javaclass.CartObj;
import ws2.productdao.ProductDAO;

/**
 *
 * @author ADMIN
 */
public class StoreMobileAction {

    private Map<String, String> mobileList;
    private final String SUCCESS = "success";

    public Map<String, String> getMobileList() {
        return mobileList;
    }

    public StoreMobileAction() {

    }

    public String execute() throws Exception {
        ProductDAO dao = new ProductDAO();
        dao.getAllProduct();
        this.mobileList = dao.getMobileList();
        return SUCCESS;
    }

}
