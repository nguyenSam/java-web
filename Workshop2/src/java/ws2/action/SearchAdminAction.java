/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws2.action;

import java.util.List;
import ws1.tblmobiledao.tblMobileDAO;
import ws1.tblmobiledao.tblMobileDTO;
import ws1.tbluserdto.tblUserDTO;

/**
 *
 * @author ADMIN
 */
public class SearchAdminAction {

    private String searchValue;
    List<tblMobileDTO> listMobile;
    private String SUCCESS = "success";

    public List<tblMobileDTO> getListMobile() {
        return listMobile;
    }

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    public SearchAdminAction() {
    }

    public String execute() throws Exception {
        tblMobileDAO dao = new tblMobileDAO();
        dao.searchValue(searchValue, searchValue);
        listMobile = dao.getListDTO();
        String url = SUCCESS;
        return url;
    }

}
