/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws2.action;

import ws1.tblmobiledao.tblMobileDAO;

/**
 *
 * @author ADMIN
 */
public class UpdateRecordAction {

    private String mobileID;
    private String description;
    private String txtPrice;
    private String txtQuantity;
    private String cbSale;
    private String SUCCESS = "success";
    private String FAIL = "fail";
    private String lastSearchValue;

    public UpdateRecordAction() {
    }

    public String execute() throws Exception {
        tblMobileDAO dao = new tblMobileDAO();
        Float price = Float.parseFloat(getTxtPrice());
        int quantity = Integer.parseInt(getTxtQuantity());
        boolean sale = false;
        String url = getFAIL();
        if (getCbSale() != null) {
            sale = true;
        }
        boolean result = dao.updateMobile(getMobileID(), getDescription(), price, quantity, sale);
        if (result) {
            url = getSUCCESS();
        }
        return url;

    }

    /**
     * @return the mobileID
     */
    public String getMobileID() {
        return mobileID;
    }

    /**
     * @param mobileID the mobileID to set
     */
    public void setMobileID(String mobileID) {
        this.mobileID = mobileID;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the txtPrice
     */
    public String getTxtPrice() {
        return txtPrice;
    }

    /**
     * @param txtPrice the txtPrice to set
     */
    public void setTxtPrice(String txtPrice) {
        this.txtPrice = txtPrice;
    }

    /**
     * @return the txtQuantity
     */
    public String getTxtQuantity() {
        return txtQuantity;
    }

    /**
     * @param txtQuantity the txtQuantity to set
     */
    public void setTxtQuantity(String txtQuantity) {
        this.txtQuantity = txtQuantity;
    }

    /**
     * @return the cbSale
     */
    public String getCbSale() {
        return cbSale;
    }

    /**
     * @param cbSale the cbSale to set
     */
    public void setCbSale(String cbSale) {
        this.cbSale = cbSale;
    }

    /**
     * @return the SUCCESS
     */
    public String getSUCCESS() {
        return SUCCESS;
    }

    /**
     * @param SUCCESS the SUCCESS to set
     */
    public void setSUCCESS(String SUCCESS) {
        this.SUCCESS = SUCCESS;
    }

    /**
     * @return the FAIL
     */
    public String getFAIL() {
        return FAIL;
    }

    /**
     * @param FAIL the FAIL to set
     */
    public void setFAIL(String FAIL) {
        this.FAIL = FAIL;
    }

    /**
     * @return the lastSearchValue
     */
    public String getLastSearchValue() {
        return lastSearchValue;
    }

    /**
     * @param lastSearchValue the lastSearchValue to set
     */
    public void setLastSearchValue(String lastSearchValue) {
        this.lastSearchValue = lastSearchValue;
    }

}
