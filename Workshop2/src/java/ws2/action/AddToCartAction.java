/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws2.action;

import com.opensymphony.xwork2.ActionContext;
import java.util.Map;
import ws1.javaclass.CartObj;
import ws2.productdao.ProductDAO;
import ws2.productdao.ProductDTO;

/**
 *
 * @author ADMIN
 */
public class AddToCartAction {

    private String mobile;
    private final String SUCCESS = "success";
    private float price;
    private final String FAIL = "fail";
    private Map<String, String> mobileList;

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobile() {
        return mobile;
    }

    public Map<String, String> getMobileList() {
        return mobileList;
    }

    public AddToCartAction() {
    }

    public String execute() throws Exception {
        Map session = ActionContext.getContext().getSession();
        float total = 0;
        CartObj co = (CartObj) session.get("CART");

        if (co == null) {
            co = new CartObj();
        }
        ProductDAO dao = new ProductDAO();
        ProductDTO dto = dao.getProduct(mobile);

        //dto.setPrice(total);
        co.addMobile(dto);
        session.put("CART", co);
        dao.getAllProduct();
        this.mobileList = dao.getMobileList();
        return SUCCESS;

    }

}
