/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws2.action;

import java.util.List;
import ws1.tblmobiledao.tblMobileDAO;
import ws1.tblmobiledao.tblMobileDTO;

/**
 *
 * @author ADMIN
 */
public class SearchPriceAction {

    private String searchPriceMin;
    private String searchPriceMax;
    private String SUCCESS = "success";
    private String FAIL = "fail";
    List<tblMobileDTO> listDto;

    public List<tblMobileDTO> getListDto() {
        return listDto;
    }

    public SearchPriceAction() {
    }

    public String execute() throws Exception {
        tblMobileDAO dao = new tblMobileDAO();
        float min = Float.parseFloat(searchPriceMin);
        float max = Float.parseFloat(searchPriceMax);
        dao.searchPrice(min, max);
        listDto = dao.getListDTO();
        String url = SUCCESS;
        return url;
    }

    /**
     * @return the searchPriceMin
     */
    public String getSearchPriceMin() {
        return searchPriceMin;
    }

    /**
     * @param searchPriceMin the searchPriceMin to set
     */
    public void setSearchPriceMin(String searchPriceMin) {
        this.searchPriceMin = searchPriceMin;
    }

    /**
     * @return the searchPriceMax
     */
    public String getSearchPriceMax() {
        return searchPriceMax;
    }

    /**
     * @param searchPriceMax the searchPriceMax to set
     */
    public void setSearchPriceMax(String searchPriceMax) {
        this.searchPriceMax = searchPriceMax;
    }

}
