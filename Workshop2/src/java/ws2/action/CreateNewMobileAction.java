/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws2.action;

import com.opensymphony.xwork2.ActionSupport;
import ws1.tblmobiledao.tblMobileDAO;

/**
 *
 * @author ADMIN
 */
public class CreateNewMobileAction extends ActionSupport {

    private String mobileID;
    private String description;
    private float price;
    private String mobileName;
    private int year;
    private int quantity;
    private boolean sale;
    private final String SUCCESS = "success";
    private final String FAIL = "fail";

    public CreateNewMobileAction() {
    }

    @Override
    public String execute() throws Exception {
        tblMobileDAO dao = new tblMobileDAO();
        boolean result = dao.insertMobile(mobileID, description, price, mobileName, year, quantity, sale);
        String url = FAIL;
        if (result) {
            url = SUCCESS;
        }
        return url;
    }

    /**
     * @return the mobileID
     */
    public String getMobileID() {
        return mobileID;
    }

    /**
     * @param mobileID the mobileID to set
     */
    public void setMobileID(String mobileID) {
        this.mobileID = mobileID;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the price
     */
    public float getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(float price) {
        this.price = price;
    }

    /**
     * @return the mobileName
     */
    public String getMobileName() {
        return mobileName;
    }

    /**
     * @param mobileName the mobileName to set
     */
    public void setMobileName(String mobileName) {
        this.mobileName = mobileName;
    }

    /**
     * @return the year
     */
    public int getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the sale
     */
    public boolean isSale() {
        return sale;
    }

    /**
     * @param sale the sale to set
     */
    public void setSale(boolean sale) {
        this.sale = sale;
    }

    /**
     * @return the SUCCESS
     */
    public String getSUCCESS() {
        return SUCCESS;
    }

    /**
     * @param SUCCESS the SUCCESS to set
     */
    /**
     * @return the FAIL
     */
    public String getFAIL() {
        return FAIL;
    }

    /**
     * @param FAIL the FAIL to set
     */
}
