/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws2.action;

import ws1.tblmobiledao.tblMobileDAO;

/**
 *
 * @author ADMIN
 */
public class DeleteRecordAction {

    private String pk;
    private String lastSearchValue;
    private String SUCCESS = "success";
    private String FAIL = "fail";

    public DeleteRecordAction() {
    }

    public String execute() throws Exception {
        tblMobileDAO dao = new tblMobileDAO();
        boolean result = dao.deleteMobile(pk);
        String url = FAIL;
        if (result) {
            url = SUCCESS;
        }
        return url;
    }

    /**
     * @return the pk
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk the pk to set
     */
    public void setPk(String pk) {
        this.pk = pk;
    }

    /**
     * @return the lastSearchValue
     */
    public String getLastSearchValue() {
        return lastSearchValue;
    }

    /**
     * @param lastSearchValue the lastSearchValue to set
     */
    public void setLastSearchValue(String lastSearchValue) {
        this.lastSearchValue = lastSearchValue;
    }

    /**
     * @return the SUCCESS
     */
    public String getSUCCESS() {
        return SUCCESS;
    }

    /**
     * @param SUCCESS the SUCCESS to set
     */
    public void setSUCCESS(String SUCCESS) {
        this.SUCCESS = SUCCESS;
    }

    /**
     * @return the FAIL
     */
    public String getFAIL() {
        return FAIL;
    }

    /**
     * @param FAIL the FAIL to set
     */
    public void setFAIL(String FAIL) {
        this.FAIL = FAIL;
    }

}
