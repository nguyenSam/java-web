/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws2.action;

import com.opensymphony.xwork2.ActionContext;
import java.util.Map;
import ws1.javaclass.CartObj;

/**
 *
 * @author ADMIN
 */
public class RemoveMobileAction {

    private String[] selectedMobile;
    private String SUCCESS = "success";

    public String[] getSelectedMobile() {
        return selectedMobile;
    }

    public void setSelectedMobile(String[] selectedMobile) {
        this.selectedMobile = selectedMobile;
    }

    public RemoveMobileAction() {
    }

    public String execute() throws Exception {
        Map session = ActionContext.getContext().getSession();
        CartObj co = (CartObj) session.get("CART");
        if (co != null) {
            for (String mobile : selectedMobile) {
                System.out.println(mobile);
                co.removeMobile(mobile);
            }
            session.put("CART", co);
        }
        return SUCCESS;
    }

}
