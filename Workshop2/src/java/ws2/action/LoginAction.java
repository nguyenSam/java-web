/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws2.action;

import com.opensymphony.xwork2.ActionContext;
import java.util.Map;
import ws1.tblmobiledao.tblMobileDAO;
import ws1.tbluserdto.tblUserDTO;

/**
 *
 * @author ADMIN
 */
public class LoginAction {

    private String username;
    private String password;
    private String SUCCESSUSER = "successUser";
    private String SUCCESSADMIN = "successAdmin";
    private String FAIL = "fail";

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LoginAction() {
    }

    public String execute() throws Exception {
        tblMobileDAO dao = new tblMobileDAO();
        tblUserDTO dto = new tblUserDTO();
        dto.setUsername(username);
        dto.setPassword(password);
        boolean result = dao.checkLogin(username, password);
        String url = FAIL;
        if (result) {
            if (dao.checkRole(dto).equals("User")) {
                Map session = ActionContext.getContext().getSession();
                session.put("USERNAME", username);
                url = SUCCESSUSER;

            } else if (dao.checkRole(dto).equals("Admin")) {
                Map session = ActionContext.getContext().getSession();
                session.put("USERNAME", username);
                url = SUCCESSADMIN;
            }
        }

        return url;
    }

}
