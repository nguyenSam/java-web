<%-- 
    Document   : searchUser
    Created on : Jun 30, 2018, 12:27:40 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search User</title>
    </head>
    <body>

        <h1>Search Page</h1>
        <br/>
        <h1>
            <font color="red">
            Welcome, ${sessionScope.USERNAME}
            </font>
        </h1>
        <s:form action="searchPrice" method="GET">

            Search Price : <s:textfield name="searchPriceMin"/>  <s:textfield name="searchPriceMax"/>
            <s:submit value="Search" />
        </s:form>
        <br/>
        <s:if test="searchPriceMin != null and searchPriceMax != null and searchPriceMin != '' and searchPriceMax != '' ">
            <s:if test="listDto != null">
                <table border="1">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>MobileId</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>MobileName</th>
                            <th>Year</th>
                            <th>Quantity</th>
                            <th>NotSale</th>
                        </tr>
                    </thead>
                    <tbody>
                        <s:iterator var="dto" value="listDto" status="counter">
                            <tr>
                                <td>
                                    <s:property value="%{#counter.count}"/>

                                </td>
                                <td>
                                    <s:property value="%{#dto.mobileID}"/>

                                </td>
                                <td>
                                    <s:property value="%{#dto.description}"/>
                                </td>
                                <td>
                                    <s:property value="%{#dto.price}"/>
                                </td>
                                <td>
                                    <s:property value="%{#dto.mobileName}"/>
                                </td>
                                <td>
                                    <s:property value="%{#dto.year}"/>
                                </td>
                                <td>
                                    <s:property value="%{#dto.quantity}"/>
                                </td>
                                <td>
                                    <s:property value="%{#dto.notSale}"/>
                                </td>
                            </tr>
                        </s:iterator>
                        </tbody>
                    </table>

            </s:if>
        </s:if>
        <s:a href="addMobile">Buy mobile</s:a>
    </body>
</html>
