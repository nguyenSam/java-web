<%-- 
    Document   : searchAdmin
    Created on : Jun 30, 2018, 12:27:54 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>
            <font color="red">
            Welcome, ${sessionScope.USERNAME}
            </font>
        </h1>
        <s:form action="search" method="GET">
              <s:textfield name="searchValue" label="Search Value"/>
            <s:submit value="Search" />
        </s:form>
            <br/>
            <s:if test="searchValue != null and searchValue != '' ">
                <s:if test="listMobile != null">
                     <table border="1">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>MobileId</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>MobileName</th>
                            <th>Year</th>
                            <th>Quantity</th>
                            <th>NotSale</th>
                            <th>Delete</th>
                            <th>Update</th>
                        </tr>
                    </thead>
                    <tbody>
                        <s:form action="updateRecord" method="GET" theme="simple">
                            
                       
                        <s:iterator var="dto" value="listMobile" status="counter">
                            <tr>
                                <td>
                                      <s:property value="%{#counter.count}"/>
                                      
                                </td>
                                <td>
                                      <s:property value="%{#dto.mobileID}"/>
                                      <s:hidden name="mobileID" value="%{#dto.mobileID}"/>
                                </td>
                                <td>
                                      <s:textfield name="description" value="%{#dto.description}" />
                                </td>
                                <td>
                                     <s:textfield name="txtPrice" value="%{#dto.price}" />
                                </td>
                                <td>
                                      <s:property value="%{#dto.mobileName}"/>
                                </td>
                                <td>
                                      <s:property value="%{#dto.year}"/>
                                </td>
                                <td>
                                       <s:textfield name="txtQuantity" value="%{#dto.quantity}" />
                                </td>
                                <td>
                                    <s:checkbox name="cbSale" value="%{#dto.notSale}"/>
                                </td>
                                <td>
                                   <s:url var="deleteLink" value="deleteRecord">
                                        <s:param name="pk" value="%{#dto.mobileID}"/>
                                        <s:param name="lastSearchValue" value="searchValue" />
                                    </s:url>
                                    <s:a href="%{deleteLink}">
                                        Delete
                                    </s:a>
                                </td>
                                 <td>
                                    <s:hidden name="lastSearchValue" value="%{searchValue}"/>
                                    <s:submit value="Update" />
                                </td>
                            </tr>
                            
                        </s:iterator>
                             </s:form>
                    </tbody>
                      </table>
                </s:if>
                 <s:if test="%{listMobile == null}">
                <h2>
                    No record is matched!!!
                </h2>
            </s:if>
            </s:if>
            
            <a href="addNew">Click here to add mobile</a>
    </body>
</html>
