<%-- 
    Document   : ViewCart
    Created on : Jul 3, 2018, 8:55:49 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Mobile Store</title>
    </head>
    <body>
        <s:if test="%{#session.CART != null}">
            <table border="1">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Action</th>

                    </tr>
                </thead>
                <tbody>
                    <s:form action="removeMobile" theme="simple">

                        <s:iterator value="%{#session.CART.mobiles}" status="counter">

                            <tr>                                                           
                                <td>
                                    <s:property value="%{#counter.count}" />

                                </td>
                                <td>
                                    <s:property value="key.mobileName"/>

                                </td>
                                <td>
                                    <s:property value="value"/>

                                </td>
                                <td>
                                    <s:if test="%{value == 1}">
                                        <s:property value="key.price"/>
                                    </s:if>

                                    <s:if test="%{value > 1}">
                                        <s:property value="key.price*value"/>
                                    </s:if>


                                </td>
                                <td>
                                    <s:checkbox name="selectedMobile" fieldValue="%{key.mobileID}"/>

                                </td>

                            </tr>
                        </s:iterator>


                        <tr>
                            <td colspan="4">
                                <s:a href="addMore">
                                    Add more mobile to your cart
                                </s:a>
                            </td>
                            <td>
                                <s:submit value="Remove Mobile"/>
                            </td>
                        </tr>
                    </s:form>
                </tbody>
            </table>

        </s:if>
        <s:if test="%{#session.CART == null}">
            No cart to view
        </s:if>
        <s:form action="storeToDB" method="GET">
            <s:submit value="Store to Database"/>
        </s:form>
    </body>
</html>
