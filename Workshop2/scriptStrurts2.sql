USE [master]
GO
/****** Object:  Database [tblMobile]    Script Date: 8/7/2018 5:02:02 PM ******/
CREATE DATABASE [tblMobile]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'WorkShop1', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLSAMEXPRESS\MSSQL\DATA\WorkShop1.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'WorkShop1_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLSAMEXPRESS\MSSQL\DATA\WorkShop1_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [tblMobile] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [tblMobile].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [tblMobile] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [tblMobile] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [tblMobile] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [tblMobile] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [tblMobile] SET ARITHABORT OFF 
GO
ALTER DATABASE [tblMobile] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [tblMobile] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [tblMobile] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [tblMobile] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [tblMobile] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [tblMobile] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [tblMobile] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [tblMobile] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [tblMobile] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [tblMobile] SET  DISABLE_BROKER 
GO
ALTER DATABASE [tblMobile] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [tblMobile] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [tblMobile] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [tblMobile] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [tblMobile] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [tblMobile] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [tblMobile] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [tblMobile] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [tblMobile] SET  MULTI_USER 
GO
ALTER DATABASE [tblMobile] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [tblMobile] SET DB_CHAINING OFF 
GO
ALTER DATABASE [tblMobile] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [tblMobile] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [tblMobile] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [tblMobile] SET QUERY_STORE = OFF
GO
USE [tblMobile]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [tblMobile]
GO
/****** Object:  Table [dbo].[tblMobile]    Script Date: 8/7/2018 5:02:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMobile](
	[mobileID] [varchar](10) NOT NULL,
	[description] [varchar](250) NOT NULL,
	[price] [float] NULL,
	[mobileName] [varchar](20) NOT NULL,
	[yearOfProduction] [int] NULL,
	[quantity] [int] NULL,
	[notSale] [bit] NULL,
 CONSTRAINT [PK_tbl_Mobile] PRIMARY KEY CLUSTERED 
(
	[mobileID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblOrder]    Script Date: 8/7/2018 5:02:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblOrder](
	[orderId] [int] NOT NULL,
	[userID] [nvarchar](20) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblOrderDetails]    Script Date: 8/7/2018 5:02:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblOrderDetails](
	[orderId] [int] NOT NULL,
	[mobileName] [nvarchar](20) NULL,
	[dateBuy] [date] NULL,
	[quantity] [int] NULL,
 CONSTRAINT [PK_tblOrderDetails] PRIMARY KEY CLUSTERED 
(
	[orderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblUser]    Script Date: 8/7/2018 5:02:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUser](
	[userID] [varchar](20) NOT NULL,
	[password] [int] NOT NULL,
	[fullName] [varchar](50) NOT NULL,
	[role] [int] NULL,
 CONSTRAINT [PK_tbl_User] PRIMARY KEY CLUSTERED 
(
	[userID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[tblMobile] ([mobileID], [description], [price], [mobileName], [yearOfProduction], [quantity], [notSale]) VALUES (N'003', N'samsung', 45, N'Z3', 1997, 12, 0)
INSERT [dbo].[tblMobile] ([mobileID], [description], [price], [mobileName], [yearOfProduction], [quantity], [notSale]) VALUES (N'APPLE', N'APPLE IS THE BEST', 8100, N'IPHONEX', 2018, 15, 1)
INSERT [dbo].[tblMobile] ([mobileID], [description], [price], [mobileName], [yearOfProduction], [quantity], [notSale]) VALUES (N'E002', N'new', 4546, N'dell', 2031, 34, 1)
INSERT [dbo].[tblMobile] ([mobileID], [description], [price], [mobileName], [yearOfProduction], [quantity], [notSale]) VALUES (N'E003', N'new`', 2342, N'iphone', 2034, 356, 0)
INSERT [dbo].[tblMobile] ([mobileID], [description], [price], [mobileName], [yearOfProduction], [quantity], [notSale]) VALUES (N'E004 ', N'old', 3434, N'samsung', 2012, 343, 1)
INSERT [dbo].[tblMobile] ([mobileID], [description], [price], [mobileName], [yearOfProduction], [quantity], [notSale]) VALUES (N'E005', N'new', 434, N'apple', 1998, 5, 0)
INSERT [dbo].[tblMobile] ([mobileID], [description], [price], [mobileName], [yearOfProduction], [quantity], [notSale]) VALUES (N'M001', N'new', 755, N'sony', 1025, 54, 0)
INSERT [dbo].[tblMobile] ([mobileID], [description], [price], [mobileName], [yearOfProduction], [quantity], [notSale]) VALUES (N'OPPO', N'OPPO SON TUNG', 8000, N'OPPO S7', 2015, 15, 0)
INSERT [dbo].[tblOrder] ([orderId], [userID]) VALUES (81, N'user1')
INSERT [dbo].[tblOrder] ([orderId], [userID]) VALUES (62, N'user1')
INSERT [dbo].[tblOrder] ([orderId], [userID]) VALUES (90, N'user1')
INSERT [dbo].[tblOrder] ([orderId], [userID]) VALUES (6, N'user1')
INSERT [dbo].[tblOrderDetails] ([orderId], [mobileName], [dateBuy], [quantity]) VALUES (6, N'OPPO S7', CAST(N'2018-06-23' AS Date), 1)
INSERT [dbo].[tblOrderDetails] ([orderId], [mobileName], [dateBuy], [quantity]) VALUES (62, N'OPPO S7', CAST(N'2018-06-23' AS Date), 1)
INSERT [dbo].[tblOrderDetails] ([orderId], [mobileName], [dateBuy], [quantity]) VALUES (81, N'IPHONEX', CAST(N'2018-06-23' AS Date), 1)
INSERT [dbo].[tblOrderDetails] ([orderId], [mobileName], [dateBuy], [quantity]) VALUES (90, N'IPHONEX', CAST(N'2018-06-23' AS Date), 1)
INSERT [dbo].[tblUser] ([userID], [password], [fullName], [role]) VALUES (N'phat', 123, N'nguyen phat', 1)
INSERT [dbo].[tblUser] ([userID], [password], [fullName], [role]) VALUES (N'sam', 123, N'nguyen khac sam', 1)
INSERT [dbo].[tblUser] ([userID], [password], [fullName], [role]) VALUES (N'sam2', 123456, N'ádasd', 0)
INSERT [dbo].[tblUser] ([userID], [password], [fullName], [role]) VALUES (N'tinh', 123, N'ad', 0)
INSERT [dbo].[tblUser] ([userID], [password], [fullName], [role]) VALUES (N'user1', 123456, N'phat', 0)
INSERT [dbo].[tblUser] ([userID], [password], [fullName], [role]) VALUES (N'user2', 123456, N'duy', 1)
INSERT [dbo].[tblUser] ([userID], [password], [fullName], [role]) VALUES (N'user3', 123456, N'qtv', 2)
INSERT [dbo].[tblUser] ([userID], [password], [fullName], [role]) VALUES (N'user4', 123456, N'tien', 0)
ALTER TABLE [dbo].[tblOrder]  WITH CHECK ADD  CONSTRAINT [FK_tblOrder_tblOrderDetails] FOREIGN KEY([orderId])
REFERENCES [dbo].[tblOrderDetails] ([orderId])
GO
ALTER TABLE [dbo].[tblOrder] CHECK CONSTRAINT [FK_tblOrder_tblOrderDetails]
GO
USE [master]
GO
ALTER DATABASE [tblMobile] SET  READ_WRITE 
GO
