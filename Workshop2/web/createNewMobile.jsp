<%-- 
    Document   : createNewMobile
    Created on : Jun 30, 2018, 8:06:22 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add new mobile</title>  
    <s:head/>
    </head>
    <body>
        <h1> Add new Mobile </h1>
        <s:form action="insert" method="GET">
            <s:textfield name="mobileID" label="Mobile ID"/>
            <s:textfield name="description" label="Description"/>
            <s:textfield name="price" label="Price"/>
            <s:textfield name="mobileName" label="Mobile Name"/>
            <s:textfield name="year" label="Year"/>
            <s:textfield name="quantity" label="Quantity"/>
            <s:checkbox name="sale" label="Sale"/>
            <s:submit value="Add new mobile"/>
            <s:reset value="Reset"/>
        </s:form>
    </body>
</html>
