<%-- 
    Document   : mobileStore.jsp
    Created on : Jul 3, 2018, 2:48:09 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mobile Store Page</title>
    </head>
    <body>
        <s:form action="addMobile">
            <s:select name="mobile" list="mobileList" label="Choose Mobile"/>
            <s:submit value="Add selected mobile to your cart"/>
        </s:form>
        <br/>
        <s:a href="viewCart">View your cart</s:a>
    </body>
</html>
