<%-- 
    Document   : myDataGrid
    Created on : Jun 20, 2018, 10:32:23 AM
    Author     : ADMIN
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="url"%>
<%@attribute name="driver"%>
<%@attribute name="user"%>
<%@attribute name="password"%>
<%@attribute name="dataSource"%>
<%@attribute name="sql"%>
<%@tag dynamic-attributes="dynaParams" %>
<c:if test="${not empty url and not empty driver}">
    <sql:setDataSource url="${url}" driver="${driver}" user="${user}"
                       password="${password}" var="con"
                       />
</c:if>
<c:if test="${not empty dataSource}">
    <sql:setDataSource dataSource="${dataSource}" var="con"
                       />
</c:if>
<c:if test="${not empty con}">
    <sql:query dataSource="${con}" var="rs">
        ${sql}
        <c:if test="${not empty dynaParams}">
            <c:forEach var="dynaParam" items="${dynaParams}">
                <sql:param value="${dynaParam.value}"/>
            </c:forEach>                         
        </c:if>
    </sql:query>
    <c:if test="${empty rs.rowsByIndex}">
        <h2>
            No record is matched!!!
        </h2>
    </c:if>
    <c:if test="${not empty rs}">


  <c:if test="${not empty rs.rowsByIndex}">
        <table border="1">
            <thead>
                <tr>
                    <th>No.</th>
                        <c:forEach var="colName" items="${rs.columnNames}">
                        <th>${colName}</th>
                        </c:forEach>
                    <th>RejectReason</th>
                    <th>Update</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <c:set var="count" value="0" />
                <c:forEach var="row" items="${rs.rowsByIndex}" >
                    <c:set var="count" value="${count+1}" />
                <form action="Status">
                    <tr>
                        <td>${count}</td>

                        <td>
                            ${row[0]}
                            <input type="hidden" name="employeeID" value="${row[0]}" />
                        </td>
                        <td>
                            ${row[1]}
                        </td>
                        <td>
                            ${row[2]}
                        </td>
                        <td>
                            ${row[3]}
                        </td>
                        <td>
                            ${row[4]}
                        </td>
                        <td>
                            ${row[5]}
                        </td>
                        <td>
                            ${row[6]}
                        </td>

                        <td>
                            <input type="text" name="txtRejectReason" value="${param.txtRejectReason}" />
                        </td>

                        <td>
                            <input type="submit" value="Accept" name="btAction" /> <br/>
                            <input type="submit" value="Reject" name="btAction" />
                        </td>

                        <td>   
                            <c:url var="changeLink" value="ChangeDep">
                                <c:param name="btAction" value="Change"/>
                                <c:param name="pk" value="${row[0]}"/>

                            </c:url> 

                            <a href="${changeLink}">
                                Change                                  
                            </a>
                        </td>                                                               
                    </tr>
                </form>
            </c:forEach>
        </tbody>
    </table>
    </c:if>
       

</c:if>
</c:if>

<%-- any content can be specified here e.g.: --%>
