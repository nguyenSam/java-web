/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pw1.tblemployeedao;

import java.io.Serializable;

/**
 *
 * @author ADMIN
 */
public class tblEmployeeDTO implements Serializable {

    private String employeeID;
    private String name;
    private float salary;
    private String address;
    private String email;
    private String phone;
    private String requestReason;
    private String rejectReason;
    private String depID;

    public tblEmployeeDTO(String employeeID, String name, float salary, String address, String email, String phone, String requestReason, String rejectReason) {
        this.employeeID = employeeID;
        this.name = name;
        this.salary = salary;
        this.address = address;
        this.email = email;
        this.phone = phone;
        this.requestReason = requestReason;
        this.rejectReason = rejectReason;
    }

    public tblEmployeeDTO(String employeeID, String name, String depID, float salary) {
        this.employeeID = employeeID;
        this.name = name;
        this.salary = salary;
        this.depID = depID;
        // this.email = email;
        // this.phone = phone;
        //  this.requestReason = requestReason;
        // this.rejectReason = rejectReason;
    }

    public void setDepID(String depID) {
        this.depID = depID;
    }

    public String getDepID() {
        return depID;
    }

    public tblEmployeeDTO() {
    }

    /**
     * @return the employeeID
     */
    public String getEmployeeID() {
        return employeeID;
    }

    /**
     * @param employeeID the employeeID to set
     */
    public void setEmployeeID(String employeeID) {
        this.employeeID = employeeID;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the salary
     */
    public float getSalary() {
        return salary;
    }

    /**
     * @param salary the salary to set
     */
    public void setSalary(float salary) {
        this.salary = salary;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the requestReason
     */
    public String getRequestReason() {
        return requestReason;
    }

    /**
     * @param requestReason the requestReason to set
     */
    public void setRequestReason(String requestReason) {
        this.requestReason = requestReason;
    }

    /**
     * @return the rejectReason
     */
    public String getRejectReason() {
        return rejectReason;
    }

    /**
     * @param rejectReason the rejectReason to set
     */
    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

}
