/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pw1.tblemployeedao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import pw1.database.DBUtil;

/**
 *
 * @author ADMIN
 */
public class tblEmployeeDAO implements Serializable {

    List<tblEmployeeDTO> listEmployeeDTO;
    public tblEmployeeDTO listDto;

    public tblEmployeeDTO getListDto() {
        return listDto;
    }

    public List<tblEmployeeDTO> getListEmployeeDTO() {
        return listEmployeeDTO;
    }

    public void searchValue(Date fromDate, Date toDate) throws SQLException, NamingException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBUtil.getConnection();
            if (con != null) {
                String sql = "SELECT employeeID,name,salary,address,email,phone,requestReason,rejectReason "
                        + "from Project.dbo.tbl_employee ,Project.dbo.tbl_leave "
                        + "where tbl_employee.employeeID = tbl_leave.empID and tbl_leave.fromDate> ? and "
                        + "? <tbl_leave.toDate";
                ps = con.prepareStatement(sql);
                ps.setDate(1, fromDate);
                ps.setDate(2, toDate);
                rs = ps.executeQuery();
                while (rs.next()) {
                    String employeeID = rs.getString("employeeID");
                    String name = rs.getString("name");
                    float salary = rs.getFloat("salary");
                    String address = rs.getString("address");
                    String email = rs.getString("email");
                    String phone = rs.getString("phone");
                    String request = rs.getString("requestReason");
                    String reject = rs.getString("rejectReason");
                    tblEmployeeDTO dto = new tblEmployeeDTO(employeeID, name, salary, address, email, phone, request, reject);
                    if (listEmployeeDTO == null) {
                        listEmployeeDTO = new ArrayList<>();
                    }
                    listEmployeeDTO.add(dto);
                }

            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }

    }

    public void loadEmployeeRequest(String userID) throws SQLException, NamingException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBUtil.getConnection();
            if (con != null) {
                String sql = "Select employeeID,name,depID,salary from tbl_employee where employeeID=?";
                ps = con.prepareStatement(sql);
                ps.setString(1, userID);
                rs = ps.executeQuery();
                while (rs.next()) {
                    String employeeID = rs.getString("employeeID");
                    String name = rs.getString("name");
                    String depID = rs.getString("depID");
                    float salary = rs.getFloat("salary");
                    tblEmployeeDTO dto = new tblEmployeeDTO(employeeID, name, depID, salary);
//                    if (listEmployeeDTO == null) {
//                        listEmployeeDTO = new ArrayList<tblEmployeeDTO>();
//                    }
//                    listEmployeeDTO.add(dto);
                    listDto = dto;

                }

            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }
    }

    public boolean updateEmployeeRequest(String employeeID, String name, String dep, float salary) throws SQLException, NamingException {
        Connection con = null;
        PreparedStatement ps = null;
        //ResultSet rs = null;
        try {
            con = DBUtil.getConnection();
            if (con != null) {
                String sql = "Update tbl_employee set tbl_employee.name=?,tbl_employee.depID=?,tbl_employee.salary=? where tbl_employee.employeeID=?";

                ps = con.prepareStatement(sql);
                ps.setString(1, name);
                ps.setString(2, dep);
                ps.setFloat(3, salary);
                ps.setString(4, employeeID);
                int result = ps.executeUpdate();
                if (result > 0) {
                    return true;
                }

            }
        } finally {

            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }

        return false;
    }

    public boolean updateLeaveRequest(String employeeID, Date fromDate, Date toDate, String requestReason) throws SQLException, NamingException {
        Connection con = null;
        PreparedStatement ps = null;
        //ResultSet rs = null;
        try {
            con = DBUtil.getConnection();
            if (con != null) {
                String sql = "Update tbl_leave set tbl_leave.fromDate=?,tbl_leave.toDate=?,tbl_leave.requestReason=? where tbl_leave.empID=?";
                ps = con.prepareStatement(sql);
                ps.setDate(1, fromDate);
                ps.setDate(2, toDate);
                ps.setString(3, requestReason);
                ps.setString(4, employeeID);
                int result = ps.executeUpdate();
                if (result > 0) {
                    return true;
                }

            }
        } finally {

            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }

        return false;
    }

    public boolean updateStatusReason(String employeeID, boolean accept, String reject) throws SQLException, NamingException {
        Connection con = null;
        PreparedStatement ps = null;
        //ResultSet rs = null;
        try {
            con = DBUtil.getConnection();
            if (con != null) {
                String sql = "Update tbl_leave set accept=?,rejectReason=? where tbl_leave.empID=?";
                ps = con.prepareStatement(sql);

                ps.setBoolean(1, accept);
                ps.setString(2, reject);
                ps.setString(3, employeeID);
                int result = ps.executeUpdate();
                if (result > 0) {
                    return true;
                }

            }
        } finally {

            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }

        return false;
    }

    public boolean changeEmpDepartment(String empID, String deparmentID) throws SQLException, NamingException {
        Connection con = null;
        PreparedStatement ps = null;
        //ResultSet rs = null;
        try {
            con = DBUtil.getConnection();
            if (con != null) {
                String sql = "update tbl_employee set depID = tbl_department.depID "
                        + "from tbl_department,tbl_employee where "
                        + "tbl_department.name=? and tbl_employee.employeeID=?";
                ps = con.prepareStatement(sql);
                ps.setString(1, deparmentID);
                ps.setString(2, empID);
                int result = ps.executeUpdate();
                if (result > 0) {
                    return true;
                }
            }
        } finally {

            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }

        return false;
    }

    public boolean changeEmpSalary(String empID, float salary) throws SQLException, NamingException {
        Connection con = null;
        PreparedStatement ps = null;
        //ResultSet rs = null;
        try {
            con = DBUtil.getConnection();
            if (con != null) {
                String sql = "update tbl_employee set salary = ? where tbl_employee.employeeID=?";
                ps = con.prepareStatement(sql);
                ps.setFloat(1, salary);
                ps.setString(2, empID);
                int result = ps.executeUpdate();
                if (result > 0) {
                    return true;
                }
            }
        } finally {

            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }

        return false;
    }

    public boolean checkLeaveExists(String empID) throws SQLException, NamingException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBUtil.getConnection();
            if (con != null) {
                String sql = "Select * from tbl_leave where empID='" + empID + "'";
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                if (rs.next()) {
                    return true;
                }
            }

        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }

        return false;
    }

    public boolean insertNewLeave(int leaveID, Date fromDate, Date toDate, boolean accept, String empID, String requestReason, String rejectReason) throws SQLException, NamingException {
        Connection con = null;
        PreparedStatement ps = null;
        //ResultSet rs = null;
        try {
            con = DBUtil.getConnection();
            if (con != null) {
                String sql = "insert into tbl_leave (leaveID,fromDate,toDate,accept,empID,requestReason,rejectReason) "
                        + "values(?,?,?,?,?,?,?)";
                ps = con.prepareStatement(sql);
                ps.setInt(1, leaveID);
                ps.setDate(2, fromDate);
                ps.setDate(3, toDate);
                ps.setBoolean(4, accept);
                ps.setString(5, empID);
                ps.setString(6, requestReason);
                ps.setString(7, rejectReason);
                int result = ps.executeUpdate();
                if (result > 0) {
                    return true;
                }
            }
        } finally {

            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }

        return false;
    }

    public boolean insertEmployee(String employeeID, String empName, String address, String email, String phone, boolean manager, String depID, float salary) throws SQLException, NamingException {
        Connection conn = null;
        PreparedStatement pstm = null;

        try {
            //sau khi mo ket noi phai getConnection de ket noi
            conn = DBUtil.getConnection();
            if (conn != null) {
                // tao cau lenh truy van
                String sql = "Insert into tbl_employee (employeeID,name,address,email,phone,manager,depID,salary) "
                        + "values(?,?,?,?,?,?,?,?)";
                pstm = conn.prepareStatement(sql);
                pstm.setString(1, employeeID);
                //System.out.println(employeeID);
                pstm.setString(2, empName);
                pstm.setString(3, address);
                pstm.setString(4, email);
                pstm.setString(5, phone);
                pstm.setBoolean(6, manager);
                pstm.setString(7, depID);
                pstm.setFloat(8, salary);
                // thuc hien truy van, tao preparedStatement

                int row = pstm.executeUpdate();
                if (row > 0) {
                    return true;
                }

            }
        } finally {

            if (pstm != null) {
                pstm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

}
