/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pw1.tblaccountdao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import pw1.database.DBUtil;
import pw1.tblemployeedao.tblEmployeeDTO;

/**
 *
 * @author ADMIN
 */
public class tblAccountDAO implements Serializable {

    public boolean checkLogin(String username, String password) throws SQLException, NamingException {

        Connection conn = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        try {

            conn = DBUtil.getConnection();
            if (conn != null) {
                String sql = "Select * from tbl_account where accountID=? and password=?";
                pstm = conn.prepareStatement(sql);
                pstm.setString(1, username);
                pstm.setString(2, password);
                rs = pstm.executeQuery();

                if (rs.next()) {
                    return true;
                }

            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstm != null) {
                pstm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    public String checkRole(tblAccountDTO dto) throws SQLException, NamingException {
        String username = dto.getAccountID();
        String password = dto.getPassword();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBUtil.getConnection();
            if (con != null) {
                String sql = "Select accountID,password,role from tbl_account";
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    String userId = rs.getString("accountID");
                    String pass = rs.getString("password");
                    int role = rs.getInt("role");
                    if (userId.equals(username) && pass.equals(password) && role == 0) {
                        return "Employee";
                    } else if (userId.equals(username) && pass.equals(password) && role == 1) {
                        return "Manager";
                    }

                }

            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return null;
    }

    public boolean checkEmployee(String employeeID) throws SQLException, NamingException {

        Connection conn = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        try {

            conn = DBUtil.getConnection();
            if (conn != null) {
                String sql = "Select * from tbl_account where employeeID= '" + employeeID + "'";
                pstm = conn.prepareStatement(sql);
                pstm.setString(1, employeeID);
                rs = pstm.executeQuery();
                if (rs.next()) {
                    return true;
                }

            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstm != null) {
                pstm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    public String checkDate(String date) {
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            df.setLenient(false);
            df.parse(date);
            return date;
        } catch (ParseException ex) {
            return null;
        }
        // return null;

    }

    public void searchValue(Date dateFrom, Date dateTo) throws SQLException, NamingException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBUtil.getConnection();
            if (con != null) {
                String sql = "Select * from tbl_leave where fromDate<'" + dateFrom + "'" + " AND " + "'" + dateTo + "'<" + dateTo;
                ps = con.prepareStatement(sql);
                ps.setDate(1, dateFrom);
                ps.setDate(2, dateTo);
                rs = ps.executeQuery();
                while (rs.next()) {
                    // String employeeID=""

                }

            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }

    }

    public boolean insertAccount(String username, String password, int role) throws SQLException, NamingException {
        Connection conn = null;
        PreparedStatement pstm = null;

        try {
            //sau khi mo ket noi phai getConnection de ket noi
            conn = DBUtil.getConnection();
            if (conn != null) {
                // tao cau lenh truy van
                String sql = "Insert into tbl_account (accountID,password,role)"
                        + " values(?,?,?)";
                // thuc hien truy van, tao preparedStatement
                pstm = conn.prepareStatement(sql);
                pstm.setString(1, username);
                pstm.setString(2, password);
                pstm.setInt(3, role);
                int row = pstm.executeUpdate();
                if (row > 0) {
                    return true;
                }

            }
        } finally {

            if (pstm != null) {
                pstm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

}
