/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pw1.tblaccountdao;

import java.io.Serializable;

/**
 *
 * @author ADMIN
 */
public class tblAccountDTO implements Serializable {

    private String accountID;
    private String password;
    private int role;

    public tblAccountDTO(String accountID, String password, int role) {
        this.accountID = accountID;
        this.password = password;
        this.role = role;
    }

    public tblAccountDTO() {
    }

    /**
     * @return the accountID
     */
    public String getAccountID() {
        return accountID;
    }

    /**
     * @param accountID the accountID to set
     */
    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the role
     */
    public int getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(int role) {
        this.role = role;
    }

}
