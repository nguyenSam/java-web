/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pw1.javaclass;

import java.io.Serializable;

/**
 *
 * @author ADMIN
 */
public class EmployeeValidation implements Serializable {

    private String nameErr;
    private String departmentErr;
    private String salaryErr;
    private String address;
    private String phone;
    private String depID;
    private String dateFrom;
    private String dateTo;
    private String reasonErr;
    private String employeeNotExisted;
    private String emailErr;

    public String getEmailErr() {
        return emailErr;
    }

    public void setEmailErr(String emailErr) {
        this.emailErr = emailErr;
    }

    public String getPhone() {
        return phone;
    }

    public String getDepID() {
        return depID;
    }

    public String getAddress() {
        return address;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setDepID(String depID) {
        this.depID = depID;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmployeeNotExisted() {
        return employeeNotExisted;
    }

    public void setEmployeeNotExisted(String employeeNotExisted) {
        this.employeeNotExisted = employeeNotExisted;
    }

    public EmployeeValidation() {
    }

    /**
     * @return the employeeIDErr
     */
    /**
     * @param EmployeeIDErr the employeeIDErr to set
     */
    /**
     * @return the nameErr
     */
    public String getNameErr() {
        return nameErr;
    }

    /**
     * @param NameErr the nameErr to set
     */
    public void setNameErr(String NameErr) {
        this.nameErr = NameErr;
    }

    /**
     * @return the departmentErr
     */
    public String getDepartmentErr() {
        return departmentErr;
    }

    /**
     * @param DepartmentErr the departmentErr to set
     */
    public void setDepartmentErr(String DepartmentErr) {
        this.departmentErr = DepartmentErr;
    }

    /**
     * @return the salaryErr
     */
    public String getSalaryErr() {

        return salaryErr;
    }

    public void setSalaryErr(String salaryErr) {
        this.salaryErr = salaryErr;
    }

    /**
     * @return the dateFrom
     */
    public String getDateFrom() {
        return dateFrom;
    }

    /**
     * @param dateFrom the dateFrom to set
     */
    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    /**
     * @return the dateTo
     */
    public String getDateTo() {
        return dateTo;
    }

    /**
     * @param dateTo the dateTo to set
     */
    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    /**
     * @return the reasonErr
     */
    public String getReasonErr() {
        return reasonErr;
    }

    /**
     * @param reasonErr the reasonErr to set
     */
    public void setReasonErr(String reasonErr) {
        this.reasonErr = reasonErr;
    }

}
