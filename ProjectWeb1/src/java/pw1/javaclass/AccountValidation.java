/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pw1.javaclass;

import java.io.Serializable;

/**
 *
 * @author ADMIN
 */
public class AccountValidation implements Serializable {

    private String usernameErr;
    private String passwordErr;
    private String confirmErr;
    private String usernameExisted;
    private String roleErr;

    public void setRoleErr(String roleErr) {
        this.roleErr = roleErr;
    }

    public String getUsernameExisted() {
        return usernameExisted;
    }

    public String getConfirmErr() {
        return confirmErr;
    }

    public void setUsernameExisted(String usernameExisted) {
        this.usernameExisted = usernameExisted;
    }

    public void setConfirmErr(String confirmErr) {
        this.confirmErr = confirmErr;
    }

    public AccountValidation() {
    }

    public void setUsernameErr(String usernameErr) {
        this.usernameErr = usernameErr;
    }

    public String getRoleErr() {
        return roleErr;
    }

    public void setPasswordErr(String passwordErr) {
        this.passwordErr = passwordErr;
    }

    public String getUsernameErr() {
        return usernameErr;
    }

    public String getPasswordErr() {
        return passwordErr;
    }

}
