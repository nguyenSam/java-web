/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pw1.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pw1.tblemployeedao.tblEmployeeDAO;

/**
 *
 * @author ADMIN
 */
public class ConfirmServlet extends HttpServlet {

    private final String confirmPage = "ManagerLeave.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String employeeID = request.getParameter("empID");
        String departmentName = request.getParameter("selectDep");
        String price = request.getParameter("txtChangeSalary");
        String url = "";
        String button = request.getParameter("btAction");
        try {
            if (button.equals("Confirm")) {
                float salary = Float.parseFloat(price);
                tblEmployeeDAO dao = new tblEmployeeDAO();
                boolean result = dao.changeEmpDepartment(employeeID, departmentName);
                boolean result2 = dao.changeEmpSalary(employeeID, salary);
                //System.out.println(salary);
                //System.out.println(employeeID);
                //System.out.println(departmentName);
                if (result && result2) {
                    url = confirmPage;
                }
            } else if (button.equals("Cancel")) {
                url = confirmPage;
            }

        } catch (SQLException ex) {
            log("ConfirmServlet_SQL " + ex.getMessage());
        } catch (NamingException ex) {
            log("ConfirmServlet_Naming " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
