/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pw1.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import pw1.javaclass.AccountValidation;
import pw1.tblaccountdao.tblAccountDAO;

/**
 *
 * @author ADMIN
 */
public class CreateAccountServlet extends HttpServlet {

    private final String showInsertErr = "CreateNewAccount.jsp";
    private final String insertEmployeePage = "CreateNewEmployee.html";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String url = showInsertErr;
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String confirm = request.getParameter("txtConfirm");
        String vaitro = request.getParameter("txtRole");
        AccountValidation err = new AccountValidation();
        try {

            boolean error = false;
            if (username.trim().length() < 5 || username.trim().length() > 20) {
                error = true;
                err.setUsernameErr("Username lenghth requires 5-20 characters");
            }
            if (password.trim().length() < 6 || password.trim().length() > 30) {
                error = true;
                err.setPasswordErr("Password lenghth requires 6-20 characters");
            } else if (!confirm.trim().equals(password.trim())) {
                error = true;
                err.setConfirmErr("Confirm must match password");

            }
            if (!vaitro.matches("[0-1]")) {
                error = true;
                err.setRoleErr("Role is 0 or 1");
            }
            if (error) {
                request.setAttribute("ERROR", err);
            } else {
                tblAccountDAO dao = new tblAccountDAO();
                int role = Integer.parseInt(vaitro);
                boolean result = dao.insertAccount(username, password, role);
                if (result) {
                    url = insertEmployeePage;
                    HttpSession session = request.getSession();
                    session.setAttribute("EMPID", username);
                }
            }

        } catch (SQLException ex) {
            log("CreateAccountServlet_SQL " + ex.getMessage());
            err.setUsernameExisted(username + " is Existed!!!");
            request.setAttribute("ERROR", err);
        } catch (NamingException ex) {
            log("CreateAccountServlet_Naming " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
