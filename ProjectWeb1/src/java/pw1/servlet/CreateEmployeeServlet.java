/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pw1.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pw1.javaclass.EmployeeValidation;
import pw1.tblemployeedao.tblEmployeeDAO;

/**
 *
 * @author ADMIN
 */
public class CreateEmployeeServlet extends HttpServlet {

    private final String showInsertEmployeeErr = "CreateNewEmployee.jsp";
    private final String managerPage = "ManagerLeave.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String url = showInsertEmployeeErr;
        HttpSession session = request.getSession(false);
        String empID = (String) session.getAttribute("EMPID");
        System.out.println(empID);
        String empName = request.getParameter("txtEmployeeName");
        String address = request.getParameter("txtAddress");
        String email = request.getParameter("txtEmail");
        String phone = request.getParameter("txtPhone");
        String role = request.getParameter("cbManager");

        boolean isManager = false;
        if (role != null) {
            isManager = true;
        }
        String txtSalary = request.getParameter("txtSalary");
        String depID = request.getParameter("txtDepID");
        EmployeeValidation empValid = new EmployeeValidation();
        try {

            boolean error = false;
            if (empName.trim().length() == 0) {
                error = true;
                empValid.setNameErr("Name cannot be empty");
                System.out.println("1");
            }
            if (address.trim().length() == 0) {
                error = true;
                empValid.setAddress("Address cannot be empty");
                System.out.println("3");
            }
            if (!email.matches("\\w+@\\w+[.]\\w+")) {
                error = true;
                empValid.setEmailErr("Email must be right with format [xxxx@xxx.xxx]");
                System.out.println("4");
            }
            if (!phone.matches("[0-9]{10}")) {
                error = true;
                empValid.setPhone("Phone must be numbers and lenght = 10 ");
                System.out.println("5");
            }
            if (!txtSalary.matches("[0-9]{1,10}")) {
                error = true;
                empValid.setSalaryErr("Salary must be numbers and cannot be empty");
                System.out.println("6");
            }
            if (!depID.equals("Dep1") && !depID.equals("Dep2") && !depID.equals("Dep3")) {
                error = true;
                empValid.setDepID("Department ID has not existed!!!");
                System.out.println("7");
            }

            if (error) {
                request.setAttribute("EMPERROR", empValid);

            } else {
                tblEmployeeDAO dao = new tblEmployeeDAO();
                float salary = Float.parseFloat(txtSalary);
                boolean result = dao.insertEmployee(empID, empName, address, email, phone, isManager, depID, salary);
                System.out.println(empID);

                if (result) {
                    url = managerPage;
                }
            }

        } catch (SQLException ex) {
            log("CreateEmployeeServlet_SQL " + ex.getMessage());
        } catch (NamingException ex) {
            log("CreateEmployeeServlet_Naming " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
