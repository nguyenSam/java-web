/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pw1.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pw1.javaclass.EmployeeValidation;
import pw1.tblaccountdao.tblAccountDAO;
import pw1.tblemployeedao.tblEmployeeDAO;
import pw1.tblemployeedao.tblEmployeeDTO;

/**
 *
 * @author ADMIN
 */
public class SearchServlet extends HttpServlet {

    private final String searchPage = "ManagerLeave.jsp";
    private final String showResult = "ManagerLeave.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String url = showResult;
        try {
            String fromDate = request.getParameter("txtSearchDateFrom");
            String toDate = request.getParameter("txtSearchDateTo");
            //String button = request.getParameter("btAction");
            boolean invalid = false;
            tblAccountDAO accountDao = new tblAccountDAO();
            EmployeeValidation error = new EmployeeValidation();
            if (accountDao.checkDate(fromDate) == null) {
                invalid = true;
                error.setDateFrom("Invalid date");
                //System.out.println("Invalid date");
            }
            if (accountDao.checkDate(toDate) == null) {
                invalid = true;
                error.setDateTo("Invalid date");
                //System.out.println("Invalid date");
            }
            if (invalid) {
                request.setAttribute("ERRORS", error);

            } else {
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                java.util.Date Datefrom = (java.util.Date) df.parse(fromDate);
                java.util.Date Dateto = (java.util.Date) df.parse(toDate);
                java.sql.Date sqlFromDate = new java.sql.Date(Datefrom.getTime());
                java.sql.Date sqlToDate = new java.sql.Date(Dateto.getTime());
                request.setAttribute("DATEFROM", sqlFromDate);
                request.setAttribute("DATETO", sqlToDate);

            }

        } catch (ParseException ex) {
            log("RequestServlet_Date " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(showResult);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
