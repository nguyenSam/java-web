/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pw1.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Random;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pw1.javaclass.EmployeeValidation;
import pw1.tblaccountdao.tblAccountDAO;
import pw1.tblemployeedao.tblEmployeeDAO;
import pw1.tblemployeedao.tblEmployeeDTO;

/**
 *
 * @author ADMIN
 */
public class RequestServlet extends HttpServlet {

    private final String errPage = "EmployeeLeave.jsp";
    private final String requestPage = "login.html";
    private final String viewList = "ViewAllLeaves.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String url = errPage;
        String employeeID = request.getParameter("txtEmployeeID");
        // System.out.println(employeeID);
        String dateFrom = request.getParameter("From");
        String dateTo = request.getParameter("To");
        //df.setLenient(false);
        String reason = request.getParameter("txtReason");
        tblEmployeeDAO employeeDao = new tblEmployeeDAO();
        tblAccountDAO accountDao = new tblAccountDAO();

        try {
            boolean invalid = false;
            EmployeeValidation error = new EmployeeValidation();
            if (accountDao.checkDate(dateFrom) == null) {
                invalid = true;
                error.setDateFrom("Invalid date");
                //System.out.println("Invalid date");
            }
            if (accountDao.checkDate(dateTo) == null) {
                invalid = true;
                error.setDateTo("Invalid date");
                //System.out.println("Invalid date");
            }
            if (reason.trim().length() == 0) {
                invalid = true;
                error.setReasonErr("Reason cannot be blanks");
                System.out.println("Reason cannot be blanks");
            }
            if (invalid) {
                request.setAttribute("ERRORS", error);

            } else {
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                java.util.Date fromDate = (java.util.Date) df.parse(dateFrom);
                java.util.Date toDate = (java.util.Date) df.parse(dateTo);
                java.sql.Date sqlFromDate = new java.sql.Date(fromDate.getTime());
                java.sql.Date sqlToDate = new java.sql.Date(toDate.getTime());
                // System.out.println("cannot1");
                boolean exists = employeeDao.checkLeaveExists(employeeID);
                // System.out.println("cannot2");
                if (exists) {
                    boolean result = employeeDao.updateLeaveRequest(employeeID, sqlFromDate, sqlToDate, reason);
                    ///System.out.println("cannot");
                    if (result) {
                        url = viewList;
                    }
                } else {
                    Random rd = new Random();
                    int leaveID = rd.nextInt(150) + 3;
                    boolean result2 = employeeDao.insertNewLeave(leaveID, sqlFromDate, sqlToDate, false, employeeID, reason, null);
                    //System.out.println("cannot");
                    if (result2) {
                        url = viewList;
                    }
                }
            }

        } catch (ParseException ex) {
            log("RequestServlet_Date " + ex.getMessage());
        } catch (SQLException ex) {
            log("RequestServlet_SQL " + ex.getMessage());
        } catch (NamingException ex) {
            log("RequestServlet_Naming " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
