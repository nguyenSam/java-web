<%-- 
    Document   : ChangeDepartment
    Created on : Jun 23, 2018, 11:04:45 AM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="myLib"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Change Department Page</title>
          <style>
              body {
                background-color: #cccccc
            }
        </style>
    </head>
    <body>

        Department Name  <myLib:depEmployeeID dataSource="SE1272"
                             sql="Select tbl_department.name from tbl_department where tbl_department.depID IN
                             (select tbl_employee.depID from tbl_employee where tbl_employee.employeeID=?)"
                             paramID="${employeeID}"
        
                             />
         <form action="Confirm">  
        <input type="hidden" name="empID" value="${employeeID}" />
        <br/>
                    
    <center>
        To Department <select name="selectDep">
            <myLib:departmentList dataSource="SE1272"
                                  sql=" Select tbl_department.name from Project.dbo.tbl_department"
                                  />
        </select>
        <br/>
        Salary <input type="text" name="txtChangeSalary" value="${param.txtChangeSalary}" />
        <br/>
        <input type="submit" value="Confirm" name="btAction" />
        <input type="submit" value="Cancel" name="btAction" /> 
         </form>
    </center>

</body>
</html>
