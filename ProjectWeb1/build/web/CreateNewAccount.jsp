<%-- 
    Document   : CreateNewAccount
    Created on : Jun 27, 2018, 2:46:20 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Create New Account</title>
          <style>
              body {
                background-color: #cccccc
            }
        </style>
    </head>
    <body>
      <h1>Create new Account</h1>
        <form action="CreateAccount" method="POST">
            Username* <input type="text" name="username" value="${param.username}" /> 
            <c:set var="error" value="${requestScope.ERROR}"/>
            <c:if test="${not empty error.usernameErr}">
                <font color="red">
                ${error.usernameErr}
                </font>
            </c:if>
            </br>
            Password* <input type="password" name="password" value="${param.password}" />  
            <c:if test="${not empty error.passwordErr}">
                <font color="red">
                ${error.passwordErr}
                </font>
            </c:if>
            </br>
            Confirm <input type="password" name="txtConfirm" value="${param.txtConfirm}" />
            <c:if test="${not empty error.confirmErr}">
                <font color="red">
                ${error.confirmErr}
                </font>
            </c:if>
                </br>
            Role* <input type="text" name="txtRole" value="${param.txtRole}" />  
            <c:if test="${not empty error.roleErr}">
                <font color="red">
                ${error.roleErr}
                </font>
            </c:if>
            </br>
            <input type="submit" value="CreateNew" name="btAction" />
            <input type="reset" value="Reset" />

        </form>
             <c:if test="${not empty error.usernameExisted}">
                <font color="red">
                ${error.usernameExisted}
                </font>
            </c:if>
    </body>
</html>
