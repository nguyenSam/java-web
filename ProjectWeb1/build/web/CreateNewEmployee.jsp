<%-- 
    Document   : CreateNewEmployee
    Created on : Jun 28, 2018, 4:33:50 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Create an employee</title>
          <style>
              body {
                background-color: #cccccc
            }
        </style>
    </head>
    <body>
        <h1>Sign up an employee</h1>
        <form action="CreateEmployee">
            Employee Name <input type="text" name="txtEmployeeName" value="${param.txtEmployeeName}" />
            <c:set var="errorEmp" value="${requestScope.EMPERROR}"/>
            <c:if test="${not empty errorEmp.nameErr}">
                <font color="red">
                ${errorEmp.nameErr}
                </font>
            </c:if>
            <br/>
            Address <input type="text" name="txtAddress" value="${param.txtAddress}" />
            <c:if test="${not empty errorEmp.address}">
                <font color="red">
                ${errorEmp.address}
                </font>
            </c:if>
            <br/>
            Email <input type="text" name="txtEmail" value="${param.txtEmail}" />
            <c:if test="${not empty errorEmp.emailErr}">
                <font color="red">
                ${errorEmp.emailErr}
                </font>
            </c:if>
            <br/>
            Phone <input type="text" name="txtPhone" value="${param.txtPhone}" />
            <c:if test="${not empty errorEmp.phone}">
                <font color="red">
                ${errorEmp.phone}
                </font>
            </c:if>
            <br/>
            Manager <input type="checkbox" name="cbManager" value="${param.cbManager}" />

            <br/>
            Department ID <input type="text" name="txtDepID" value="${param.txtDepID}" />
            <c:if test="${not empty errorEmp.depID}">
                <font color="red">
                ${errorEmp.depID}
                </font>
            </c:if>
            <br/>
            Salary <input type="text" name="txtSalary" value="${param.txtSalary}" />
            <c:if test="${not empty errorEmp.salaryErr}">
                <font color="red">
                ${errorEmp.salaryErr}
                </font>
            </c:if>
            <input type="submit" value="Create" name="btAction" />
            <input type="reset" value="Reset" />
        </form>
    </body>
</html>
