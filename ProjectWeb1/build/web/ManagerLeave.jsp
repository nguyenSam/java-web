

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="myLib"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manager Employee Leave</title>
        <style>
            body {
                background-color: #cccccc
            }
        </style>
    </head>
    <body>
              <c:set var="employee" value="${sessionScope.USERNAME}"/>
        <h1>
            <font color="red">
            Welcome Manager : ${employee}
            </font>
        </h1>

        <form action="Search">
            <h3>Department Name 
                <myLib:tagLibDepName dataSource="SE1272"
                                     sql=" Select tbl_department.name as Name from Project.dbo.tbl_department"
                                     />
            </h3> 
            <h3>Total of Leaving Employees : <myLib:taglibCountTotal dataSource="SE1272"
                                    sql="select * from Project.dbo.tbl_leave"/>   
            </h3> 

            From  <input type="text" name="txtSearchDateFrom" value="${param.txtSearchDateFrom}" />
            <c:set var="error" value="${requestScope.ERRORS}"/>
            <c:if test="${not empty error.dateFrom}">
                <font color="red">
                ${error.dateFrom}
                </font>
            </c:if>
            To <input type="text" name="txtSearchDateTo" value="${param.txtSearchDateTo}" />
            <c:if test="${not empty error.dateTo}">
                <font color="red">
                ${error.dateTo}
                </font>
            </c:if>
            <input type="submit" value="Search" name="btAction" />
            <br/>
        </form>
        <c:set var="search" value="${btAction}"/>
        <c:set var="searchFrom" value="${param.txtSearchDateFrom}" />
        <c:set var="searchTo" value="${param.txtSearchDateTo}" />
        <sql:setDataSource dataSource="SE1272" var="con"/>
    
        <c:if test="${empty searchFrom and empty searchTo and not empty search}">
            <myLib:myDataGrid dataSource="SE1272"
                              sql="SELECT employeeID,name,salary,address,email,phone,requestReason
                              from Project.dbo.tbl_employee ,Project.dbo.tbl_leave 
                              where tbl_employee.employeeID = tbl_leave.empID"                              
                              >                   
            </myLib:myDataGrid>
        </c:if>


         <c:if test="${not empty searchFrom and not empty searchTo}">
              <myLib:myDataGrid dataSource="SE1272"
                         sql="SELECT employeeID,name,salary,address,email,phone,requestReason
                         from Project.dbo.tbl_employee ,Project.dbo.tbl_leave 
                         where tbl_employee.employeeID = tbl_leave.empID and tbl_leave.fromDate<=? and ?<=tbl_leave.toDate"
                         parValue1="${searchFrom}" parValue2="${searchTo}"
                         />
         </c:if> 
        <c:if test="${empty searchFrom and empty searchTo}">
            <myLib:myDataGrid dataSource="SE1272"
                              sql="SELECT employeeID,name,salary,address,email,phone,requestReason
                              from Project.dbo.tbl_employee ,Project.dbo.tbl_leave 
                              where tbl_employee.employeeID = tbl_leave.empID"                              
                              >

            </myLib:myDataGrid>

        </c:if>


        <a href="CreateNewAccount.html">Click here to sign up an account</a>
        <form action="LogOut"><input type="submit" value="Log Out" name="btAction" /> </form>

    </body>
</html>
