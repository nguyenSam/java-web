<%-- 
    Document   : myDataGrid
    Created on : Jun 20, 2018, 10:32:23 AM
    Author     : ADMIN
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="url"%>
<%@attribute name="driver"%>
<%@attribute name="user"%>
<%@attribute name="password"%>
<%@attribute name="dataSource"%>
<%@attribute name="sql"%>
<%@tag dynamic-attributes="dynaParams" %>
<c:if test="${not empty url and not empty driver}">
    <sql:setDataSource url="${url}" driver="${driver}" user="${user}"
                       password="${password}" var="con"
                       />
</c:if>
<c:if test="${not empty dataSource}">
    <sql:setDataSource dataSource="${dataSource}" var="con"
                       />
</c:if>
<c:if test="${not empty con}">
    <sql:query dataSource="${con}" var="rs">
        ${sql}
        <c:if test="${not empty dynaParams}">
            <c:forEach var="dynaParam" items="${dynaParams}">
                <sql:param value="${dynaParam.value}"/>
            </c:forEach>                         
        </c:if>
    </sql:query>
    <c:if test="${not empty rs}">                
        <table border="1">
            <thead>
                <tr>
                    <th>No.</th>
                        <c:forEach var="colName" items="${rs.columnNames}">
                        <th>${colName}</th>
                        </c:forEach>
                      
                </tr>
            </thead>
            <tbody>
               
                <c:forEach var="row" items="${rs.rowsByIndex}" >                 
                    <tr>                    
                        <c:forEach var="field" items="${row}" >
                            <td>${field}</td>
                        </c:forEach>                        
                    </tr>
                </c:forEach>
            </tbody>
        </table>
  
    </c:if>
</c:if>

<%-- any content can be specified here e.g.: --%>
