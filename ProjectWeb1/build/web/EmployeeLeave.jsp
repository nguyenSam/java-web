<%-- 
    Document   : EmployeeLeave
    Created on : Jun 21, 2018, 7:00:58 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="myLib"%>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Employee Page</title>
          <style>
              body {
                background-color: #cccccc
            }
        </style>
    </head>
    <body>
         
        <form action="Request" >
            <c:set var="employee" value="${sessionScope.EMPLOYEE}"/>
            <c:if test="${not empty employee}">
                Employee ID : ${employee.employeeID} 
                <input type="hidden" name="txtEmployeeID" value="${employee.employeeID}" />
                <br/>
                Name : ${employee.name}<br/>
                Department : ${employee.depID}<br/>
                Salary : ${employee.salary}<br/>
            </c:if>
            Leave Date
            <br/>
            From  <input type="text" name="From" value="${param.From}" />
            To <input type="text" name="To" value="${param.To}" /> 
            <c:set var="errors" value="${ERRORS}" />
            <c:if test="${not empty errors.dateFrom}">
                <font color="red">
                ${errors.dateFrom}
                </font>
            </c:if>
            <c:if test="${not empty errors.dateTo}">
                <font color="red">
                ${errors.dateTo}
                </font>
            </c:if>
            <br/>
            Reason <input type="text" name="txtReason" value="${param.txtReason}" />
            <c:if test="${not empty errors.reasonErr}">
                <font color="red">
                ${errors.reasonErr}
                </font>
            </c:if>
            <br/>
            <input type="submit" value="Request Reason" name="btAction" />

        </form>                       
            <a href="ViewAllLeaves.jsp"> View all leaves </a> 
            <form action="LogOut"><input type="submit" value="Log Out" name="btAction" /> </form>


        <%--        Employee ID <input type="text" name="txtEmployeeID" value="${param.txtEmployeeID}" /> 
                    <c:set var="errors" value="${requestScope.ERRORS}" />
                    <c:if test="${not empty errors.employeeIDErr}">
                        <font color="red">
                        ${errors.employeeIDErr}
                        </font> 
                         </c:if>
                    <br/>
                  
                    Name <input type="text" name="txtEmployeeName" value="${param.txtEmployeeName}" /> 
                     <c:if test="${not empty errors.nameErr}">
                        <font color="red">
                        ${errors.nameErr}
                        </font>
                    </c:if>
                    <br/>
                    Department <input type="text" name="txtDepName" value="${param.txtDepName}" />
                      <c:if test="${not empty errors.departmentErr}">
                        <font color="red">
                        ${errors.departmentErr}
                        </font>
                    </c:if>
                    <br/>
                    Salary <input type="text" name="txtSalary" value="${param.txtSalary}" />
                      <c:if test="${not empty errors.salaryErr}">
                        <font color="red">
                        ${errors.salaryErr}
                        </font>
                    </c:if>
                    <br/>  --%>
    </body>
</html>
