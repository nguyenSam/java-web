USE [master]
GO
/****** Object:  Database [Project]    Script Date: 8/6/2018 9:49:34 PM ******/
CREATE DATABASE [Project]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Project', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLSAMEXPRESS\MSSQL\DATA\Project.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Project_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLSAMEXPRESS\MSSQL\DATA\Project_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Project] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Project].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Project] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Project] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Project] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Project] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Project] SET ARITHABORT OFF 
GO
ALTER DATABASE [Project] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [Project] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Project] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Project] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Project] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Project] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Project] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Project] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Project] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Project] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Project] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Project] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Project] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Project] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Project] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Project] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Project] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Project] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Project] SET  MULTI_USER 
GO
ALTER DATABASE [Project] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Project] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Project] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Project] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Project] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Project] SET QUERY_STORE = OFF
GO
USE [Project]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [Project]
GO
/****** Object:  Table [dbo].[tbl_account]    Script Date: 8/6/2018 9:49:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_account](
	[accountID] [nvarchar](20) NOT NULL,
	[password] [nvarchar](20) NOT NULL,
	[role] [int] NULL,
 CONSTRAINT [PK_tbl_account] PRIMARY KEY CLUSTERED 
(
	[accountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_department]    Script Date: 8/6/2018 9:49:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_department](
	[depID] [nvarchar](20) NOT NULL,
	[depName] [nvarchar](20) NULL,
 CONSTRAINT [PK_tbl_department] PRIMARY KEY CLUSTERED 
(
	[depID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_employee]    Script Date: 8/6/2018 9:49:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_employee](
	[employeeID] [nvarchar](20) NOT NULL,
	[name] [nvarchar](20) NULL,
	[address] [nvarchar](250) NULL,
	[email] [varchar](30) NULL,
	[phone] [varchar](11) NULL,
	[manager] [bit] NULL,
	[depID] [nvarchar](20) NULL,
	[salary] [float] NULL,
 CONSTRAINT [PK_tbl_employee] PRIMARY KEY CLUSTERED 
(
	[employeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_leave]    Script Date: 8/6/2018 9:49:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_leave](
	[leaveID] [int] NOT NULL,
	[fromDate] [datetime] NULL,
	[toDate] [datetime] NULL,
	[accept] [bit] NULL,
	[empID] [nvarchar](20) NOT NULL,
	[requestReason] [nvarchar](250) NULL,
	[rejectReason] [nvarchar](250) NULL,
 CONSTRAINT [PK_tbl_leave] PRIMARY KEY CLUSTERED 
(
	[leaveID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_roles]    Script Date: 8/6/2018 9:49:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_roles](
	[role] [int] NOT NULL,
	[description] [nvarchar](50) NULL,
 CONSTRAINT [PK_tbl_roles] PRIMARY KEY CLUSTERED 
(
	[role] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[tbl_account] ([accountID], [password], [role]) VALUES (N'phat', N'1234', 1)
INSERT [dbo].[tbl_account] ([accountID], [password], [role]) VALUES (N'sam', N'1234', 0)
INSERT [dbo].[tbl_account] ([accountID], [password], [role]) VALUES (N'tai', N'1234', 1)
INSERT [dbo].[tbl_account] ([accountID], [password], [role]) VALUES (N'thanh', N'1234', 0)
INSERT [dbo].[tbl_account] ([accountID], [password], [role]) VALUES (N'thinh', N'1234', 1)
INSERT [dbo].[tbl_account] ([accountID], [password], [role]) VALUES (N'tinh', N'1234', 1)
INSERT [dbo].[tbl_account] ([accountID], [password], [role]) VALUES (N'trang', N'1234', 0)
INSERT [dbo].[tbl_department] ([depID], [depName]) VALUES (N'D001', N'Hoang mai')
INSERT [dbo].[tbl_department] ([depID], [depName]) VALUES (N'D002', N'Noi cat')
INSERT [dbo].[tbl_department] ([depID], [depName]) VALUES (N'D003', N'Bo de')
INSERT [dbo].[tbl_employee] ([employeeID], [name], [address], [email], [phone], [manager], [depID], [salary]) VALUES (N'phat ', N'lam thanh phat', N'44 duoog 33', N'phat@gmail.com', N'325233233', 0, N'D003', 32425964)
INSERT [dbo].[tbl_employee] ([employeeID], [name], [address], [email], [phone], [manager], [depID], [salary]) VALUES (N'sam', N'nguyen khac sam', N'44 duong 18', N'sam@gmail.com', N'234234234', 1, N'D001', 3243234)
INSERT [dbo].[tbl_employee] ([employeeID], [name], [address], [email], [phone], [manager], [depID], [salary]) VALUES (N'thanh', N'nguyen hong thanh', N'93 lo 18 cong hòa', N'thanh@gmail.com', N'900349141', 1, N'D001', 39495225)
INSERT [dbo].[tbl_employee] ([employeeID], [name], [address], [email], [phone], [manager], [depID], [salary]) VALUES (N'thinh', N'le quang thinh', N'vo gia cu', N'thinh@gmail.com', N'234235452', 0, N'D001', 64634212)
INSERT [dbo].[tbl_employee] ([employeeID], [name], [address], [email], [phone], [manager], [depID], [salary]) VALUES (N'tinh', N'phan huu tinh', N'vo gia cu', N'tinh@gmail.com', N'059385823', 0, N'D002', 94852234)
INSERT [dbo].[tbl_employee] ([employeeID], [name], [address], [email], [phone], [manager], [depID], [salary]) VALUES (N'trang', N'nguyen trang', N'new york', N'trang@gmail.com', N'593459293', 1, N'D003', 23556234)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (32, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (41, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (67, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (74, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (79, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (97, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (101, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (120, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (206, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (216, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (229, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'haha   ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (234, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (243, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (266, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (272, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (404, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'hihi', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (448, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (453, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (467, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (498, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (506, CAST(N'2014-12-01T00:00:00.000' AS DateTime), CAST(N'2018-12-24T00:00:00.000' AS DateTime), NULL, N'phat', N'let go
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (561, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (594, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (604, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (605, CAST(N'2013-12-12T00:00:00.000' AS DateTime), CAST(N'2016-12-12T00:00:00.000' AS DateTime), NULL, N'phat', N'ok  bede   ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (608, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (645, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (761, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (812, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (833, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (840, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (879, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (883, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (892, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (900, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (939, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_leave] ([leaveID], [fromDate], [toDate], [accept], [empID], [requestReason], [rejectReason]) VALUES (960, CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, N'phat', N'                Enter your reason here !!!
            ', NULL)
INSERT [dbo].[tbl_roles] ([role], [description]) VALUES (0, N'manager')
INSERT [dbo].[tbl_roles] ([role], [description]) VALUES (1, N'employee')
ALTER TABLE [dbo].[tbl_employee] ADD  CONSTRAINT [DF_tbl_employee_manager]  DEFAULT ((0)) FOR [manager]
GO
ALTER TABLE [dbo].[tbl_leave] ADD  CONSTRAINT [DF_tbl_leave_fromDate]  DEFAULT (getdate()) FOR [fromDate]
GO
ALTER TABLE [dbo].[tbl_account]  WITH CHECK ADD  CONSTRAINT [FK_tbl_account_tbl_roles] FOREIGN KEY([role])
REFERENCES [dbo].[tbl_roles] ([role])
GO
ALTER TABLE [dbo].[tbl_account] CHECK CONSTRAINT [FK_tbl_account_tbl_roles]
GO
ALTER TABLE [dbo].[tbl_employee]  WITH CHECK ADD  CONSTRAINT [FK_tbl_employee_tbl_account] FOREIGN KEY([employeeID])
REFERENCES [dbo].[tbl_account] ([accountID])
GO
ALTER TABLE [dbo].[tbl_employee] CHECK CONSTRAINT [FK_tbl_employee_tbl_account]
GO
ALTER TABLE [dbo].[tbl_employee]  WITH CHECK ADD  CONSTRAINT [FK_tbl_employee_tbl_department] FOREIGN KEY([depID])
REFERENCES [dbo].[tbl_department] ([depID])
GO
ALTER TABLE [dbo].[tbl_employee] CHECK CONSTRAINT [FK_tbl_employee_tbl_department]
GO
ALTER TABLE [dbo].[tbl_leave]  WITH CHECK ADD  CONSTRAINT [FK_tbl_leave_tbl_employee] FOREIGN KEY([empID])
REFERENCES [dbo].[tbl_employee] ([employeeID])
GO
ALTER TABLE [dbo].[tbl_leave] CHECK CONSTRAINT [FK_tbl_leave_tbl_employee]
GO
USE [master]
GO
ALTER DATABASE [Project] SET  READ_WRITE 
GO
